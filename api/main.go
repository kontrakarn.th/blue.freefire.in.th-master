package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"runtime/debug"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load(
		".env",
	)
	if err != nil {
		log.Fatal("error loading .env file!")
	}

	initDbConfig()
	router := NewRouter()

	autoRecoverListenAndServe(router)
}

func autoRecoverListenAndServe(router *mux.Router) {
	httpChan := make(chan bool)
	go func() {
		defer func() {
			if r := recover(); r != nil {
				fmt.Println("stacktrace from panic: \n" + string(debug.Stack()))
				log.Print("recovered from: ", r)
			}
			httpChan <- true
		}()
		http.ListenAndServe(":"+os.Getenv("GOPORT"), handlers.RecoveryHandler()(router))
	}()
	log.Print("started listening for http connections on port: " + os.Getenv("GOPORT"))

	select {
	case <-httpChan:
		log.Print("restarting http service...")
		time.Sleep(5 * time.Second)
		autoRecoverListenAndServe(router)
	}
}

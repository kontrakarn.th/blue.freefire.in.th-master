package Redeem

import (
	// "fmt"

	"context"
	"log"
	"net/http"
	"os"
	"project/common"
	"time"

	// "strconv"

	Constant "project/app/Constant"
	"project/app/Controller"
	model "project/app/Model"
	database "project/common/database"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type UserData struct {
	Gender   int    `bson:"gender"`
	Icon     string `json:"icon"`
	Nickname string `json:"nickname"`
	Platform int    `json:"platform"`
	Uid      int    `json:"uid"`
	Username string `json:"username"`
	OpenId   string `json:"open_id"`
}

/*func Incode(w http.ResponseWriter, r *http.Request) {
	data, _, _ := model.InsertCode()

	Constant.ResponseMessage(w, data, "", true)
	return
}*/
func Redeems(w http.ResponseWriter, r *http.Request) {
	userData := common.GetUserData(r, os.Getenv("APP_SECRET"))
	// fmt.Println(userData)
	openId := userData.OpenId
	// get event

	events, msg, status := model.GetEvents(os.Getenv("EVENT_NAME"))

	if status == false {
		Constant.ResponseMessage(w, nil, msg, false)
		return
	}
	userEventInfo, msg, status := model.GetUserByOpenID(openId)
	if status == false {
		Constant.ResponseMessage(w, nil, msg, false)
		return
	}
	if userEventInfo.Ban == true {
		//ban_util := int64(userEventInfo.BanAt + userEventInfo.BanTime)
		//time_ban := Constant.UnixToTime(ban_util)
		//	time_ban_string := time_ban.Format("02/01/2006 15:04:05")
		//	Constant.ResponseMessage(w, nil, time_ban_string, false)
		//	return
	}

	req := Constant.FormJson(r.Body)
	if req == nil {
		log.Print("dont have post data")
		Constant.ResponseMessage(w, nil, Constant.GetWording("default")+" (1)", false)
		return
	}

	code := req["code"].(string)
	if code == "" {
		log.Print("wrong code :", code)
		Constant.ResponseMessage(w, nil, Constant.GetWording("default")+" (2)", false)
		return
	}
	if code == "" {
		func(u_ro model.Users) {
			UpdateWrong(u_ro)
		}(userEventInfo)
		userEventInfo, msg, status = model.GetUserByOpenID(openId)
		if status == false {
			Constant.ResponseMessage(w, nil, msg, false)
			return
		}
		//fmt.Println("userEventInfo", userEventInfo)
		if userEventInfo.Ban == true {

			ban_util := int64(userEventInfo.BanAt + userEventInfo.BanTime)
			time_ban := Constant.UnixToTime(ban_util)
			time_ban_string := time_ban.Format("02/01/2006 15:04:05")
			//fmt.Println(time_ban_string)

			Constant.ResponseMessage(w, nil, "คุณถูกแบน จะกรอกโค้ดได้อีกครั้ง "+time_ban_string, false)
			return
		}
		//log.Print("wrong code :", code)
		Constant.ResponseMessage(w, nil, Constant.GetWording("default")+" (2)", false)
		return
	}

	code_get, code_msg, code_stat := model.GetCode(code)

	if code_stat == false {
		func(u_ro model.Users) {
			UpdateWrong(u_ro)
		}(userEventInfo)
		userEventInfo, msg, status = model.GetUserByOpenID(openId)
		if status == false {
			Constant.ResponseMessage(w, nil, msg, false)
			return
		}
		//fmt.Println("userEventInfo", userEventInfo)
		if userEventInfo.Ban == true {

			ban_util := int64(userEventInfo.BanAt + userEventInfo.BanTime)
			time_ban := Constant.UnixToTime(ban_util)
			time_ban_string := time_ban.Format("02/01/2006 15:04:05")
			itemcount := (userEventInfo.RedeemMilestone) * 5 / 100
			if itemcount == 5 {
				itemcount = 0
			}

			data := map[string]interface{}{
				"account_id":   userEventInfo.AccountId,
				"account_name": userEventInfo.AccountName,
				"ban":          userEventInfo.Ban,
				"all_redeem":   userEventInfo.AllRedeem,
				"milestone":    userEventInfo.RedeemMilestone,
				"item_count":   itemcount,
				"end_at":       events.EndTime,
				"ban_end":      time_ban_string,
			}
			Constant.ResponseMessage(w, data, "คุณถูกแบน จะกรอกโค้ดได้อีกครั้ง "+time_ban_string, false)
			return
		}
		Constant.ResponseMessage(w, nil, code_msg, false)
		return
	}
	if code_get.Used == true {
		func(u_ro model.Users) {
			UpdateWrong(u_ro)
		}(userEventInfo)
		userEventInfo, msg, status = model.GetUserByOpenID(openId)
		if status == false {
			Constant.ResponseMessage(w, nil, msg, false)
			return
		}
		if userEventInfo.Ban == true {
			ban_util := int64(userEventInfo.BanAt + userEventInfo.BanTime)
			time_ban := Constant.UnixToTime(ban_util)
			time_ban_string := time_ban.Format("02/01/2006 15:04:05")
			itemcount := (userEventInfo.RedeemMilestone) * 5 / 100
			if itemcount == 5 {
				itemcount = 0
			}
			data := map[string]interface{}{
				"account_id":   userEventInfo.AccountId,
				"account_name": userEventInfo.AccountName,
				"ban":          userEventInfo.Ban,
				"all_redeem":   userEventInfo.AllRedeem,
				"milestone":    userEventInfo.RedeemMilestone,
				"item_count":   itemcount,
				"end_at":       events.EndTime,
				"ban_end":      time_ban_string,
			}
			Constant.ResponseMessage(w, data, "คุณถูกแบน จะกรอกโค้ดได้อีกครั้ง "+time_ban_string, false)
			return
		}
		Constant.ResponseMessage(w, nil, Constant.GetWording("dup_or_wrong"), false)
		return
	} else {

	}

	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		Constant.ResponseMessage(w, nil, Constant.GetWording("cantconnectdb"), false)
		return
	}

	loc, _ := time.LoadLocation(os.Getenv("TZ"))
	//n := time.Now().In(loc)
	//nformat := n.Format("2006-01-02")
	//
	myip := Constant.GetIPAdress(r)
	useragent := Constant.GetUserAgent(r)
	n := time.Now().In(loc)
	nformat := n.Format("2006-01-02")
	//accountid := userEventInfo.AccountId
	// keyaccountid := userEventInfo.AccountId
	col_code := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME") + "_code")

	go func(code_ro string, userEventInfo_ro model.Users, myip_ro string, useragent_ro string) {
		//update code used
		col_code.UpdateOne(context.TODO(), bson.M{"code": code_ro, "used": false},
			bson.M{"$set": bson.M{
				"used":               true,
				"used_by_account_id": userEventInfo_ro.AccountId,
				"used_by_name":       userEventInfo_ro.AccountName,
				"used_at":            time.Now().In(loc),
				"used_at_string":     time.Now().In(loc).Format("2006-01-02 15:04:05"),
				"ip":                 myip_ro,
				"useragent":          useragent_ro,
			}})

	}(code, userEventInfo, myip, useragent)

	allRedeem := userEventInfo.AllRedeem + 1
	RedeemMilestone := userEventInfo.RedeemMilestone + 20
	if RedeemMilestone > 100 {
		RedeemMilestone = 20
	}
	//RedeemMilestone := 1
	var item_msg string
	var item_stat bool
	var addlegendary int
	var item []model.Items
	if RedeemMilestone == 100 {
		addlegendary = 1
		item, item_msg, item_stat = model.GetTwoitem()

	} else {
		item, item_msg, item_stat = model.GetOneitem()

	}
	if item_stat == false {
		Constant.ResponseMessage(w, nil, item_msg, false)
		return
	}
	itemlist := []map[string]interface{}{}
	coins := 0
	gems := 0
	for _, it := range item {
		for _, item_data := range it.Items {
			if item_data.Type == "gold" {
				coins += item_data.Cnt
			} else if item_data.Type == "gems" {
				gems += item_data.Cnt
			} else {
				itemlist = append(itemlist, map[string]interface{}{
					"id":  item_data.Id,
					"cnt": item_data.Cnt,
				})
			}
		}
	}

	ReturnItem := []map[string]interface{}{}
	for _, returntwo := range item {
		//in := strconv.Itoa(index + 1)
		var item_image string
		var item_image_2 string
		var item_name string
		var item_no string

		item_image = "item_image"
		item_image_2 = "item_image_2"
		item_name = "item_name"
		item_no = "item_no"

		ReturnItem = append(ReturnItem, map[string]interface{}{
			item_image:   returntwo.Image,
			item_image_2: returntwo.Image2,
			item_name:    returntwo.Items[0].Name,
			item_no:      returntwo.No,
		})

	}
	go func(u_ro model.Users) {
		UpdateMilestone(u_ro)
	}(userEventInfo)

	chkunique, _, _ := model.IsUniqueAccountId(userEventInfo.AccountId)
	log.Print("chkunique", chkunique)
	go func(code_get_ro model.Code, legendary int) {
		model.ReportIncrement("total_redeem", 1)
		model.ReportIncrement("total_redeem_"+code_get_ro.CodeType, 1)
		model.ReportIncrement("total_redeem_normal", 1)
		model.ReportIncrement("total_redeem_legendary", legendary)

	}(code_get, addlegendary)
	col_sendlogs := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME") + "_sendlogs")
	//send item
	sendlogsins := &model.SendLogs{
		Id:                primitive.NewObjectID(),
		OpenId:            userEventInfo.OpenId,
		AccountId:         userEventInfo.AccountId,
		AccountName:       userEventInfo.AccountName,
		Code:              code,
		ItemList:          item,
		Status:            "pending",
		Ip:                myip,
		UserAgent:         useragent,
		CreatedAtString:   nformat,
		CreatedAtStringDT: n.Format("2006-01-02 15:04:05"),
		CreatedAt:         n,
		UpdatedAt:         n,
	}
	_, err_ins := col_sendlogs.InsertOne(context.TODO(), sendlogsins)

	if err_ins != nil {
		log.Print(err_ins)
		Constant.ResponseMessage(w, nil, Constant.GetWording("contactcs"), false)
		return
	} else {

		sendItemId := sendlogsins.Id
		go func(send_id primitive.ObjectID, userEventInfo_ro_2 model.Users, events_ro model.FreefireEvents, itemlist_ro []map[string]interface{}, coins_ro int, gems_ro int) {
			// send Item
			var datasend = map[string]interface{}{
				"account_id": userEventInfo_ro_2.AccountId,
				"title":      events_ro.MailTitle,
				"content":    events_ro.MailContent,
				"items":      itemlist_ro,
			}

			status, returnfail, response, err := Controller.SendMails(datasend, 0)

			if err != nil {
				for i := 0; i <= 3; i++ {
					if returnfail == 3 {
						log.Println("erorr|Play:SendMails|3", err)

					} else if returnfail == i {
						log.Println("erorr|Play:SendMails|", i, err)
						if returnfail == 0 {

						} else {
							status, returnfail, response, err = Controller.SendMails(datasend, 1)
						}

					}
				}

				log.Println("erorr|Play:SendMails|", err)
			}

			col_sendlogs.UpdateOne(context.TODO(), bson.M{"_id": send_id, "status": "pending"},
				bson.M{"$set": bson.M{
					"status":     status,
					"response":   response,
					"updated_at": time.Now().In(loc),
				}})
			model.ReportUniqueRedeem(userEventInfo_ro_2.AccountId)
		}(sendItemId, userEventInfo, events, itemlist, coins, gems)

	}
	//update

	query_user_where := bson.M{"account_id": userEventInfo.AccountId}
	query_user_up := bson.M{"$set": bson.M{
		"wrong":    0,
		"ban_at":   0,
		"ban_time": 0,
		"ban":      false,
	}}

	_, msg_user_up, stat_user_up := model.UpdateUser(query_user_where, query_user_up)
	if stat_user_up == false {
		Constant.ResponseMessage(w, nil, msg_user_up, false)
		return
	}
	itemcount := (RedeemMilestone) * 5 / 100
	if itemcount == 5 {
		itemcount = 0
	}
	data := map[string]interface{}{
		"item_get":   ReturnItem,
		"milestone":  RedeemMilestone,
		"all_redeem": allRedeem,
		"item_count": itemcount,
	}
	Constant.ResponseMessage(w, data, "", true)
	return

}
func UpdateWrong(user model.Users) {
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print(Constant.GetWording("cantconnectdb"))
		return
	}
	col_users := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME") + "_users")
	opts_2 := options.FindOneAndUpdate().SetUpsert(false)
	filter := bson.M{"account_id": user.AccountId}
	var update bson.M
	new_wrong := user.Wrong + 1
	if new_wrong > 5 {
		//minute -> second
		loc, _ := time.LoadLocation(os.Getenv("TZ"))
		ban_at := time.Now().In(loc).Unix()
		ban_time := new_wrong * 5 * 60
		//ban_time := new_wrong * 1 * 5

		update = bson.M{"$set": bson.M{"wrong": new_wrong, "ban": true, "ban_at": ban_at, "ban_time": ban_time}}
	} else {
		update = bson.M{"$inc": bson.M{"wrong": 1}}
	}
	var updatedDocument bson.M
	err := col_users.FindOneAndUpdate(context.TODO(), filter, update, opts_2).Decode(&updatedDocument)
	if err != nil {
		log.Print(err)
	}
}
func UpdateMilestone(user model.Users) {
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print(Constant.GetWording("cantconnectdb"))
		return
	}
	col_users := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME") + "_users")
	opts_2 := options.FindOneAndUpdate().SetUpsert(false)
	filter := bson.M{"account_id": user.AccountId}
	var update bson.M
	var redeemmilestone int
	quantityredeem := user.AllRedeem + 1
	milestonefromdb := user.RedeemMilestone
	if milestonefromdb == 100 {
		redeemmilestone = 20
	} else {
		redeemmilestone = milestonefromdb + 20
	}

	update = bson.M{"$set": bson.M{"redeem_milestone": redeemmilestone, "all_redeem": quantityredeem}}

	var updatedDocument bson.M
	err := col_users.FindOneAndUpdate(context.TODO(), filter, update, opts_2).Decode(&updatedDocument)
	if err != nil {
		log.Print(err)
	}
}

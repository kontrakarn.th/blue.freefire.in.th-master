package Controller

import (
	"errors"
	"log"
	"reflect"
	"strconv"

	// "github.com/fatih/structs"
	"os"

	Constant "project/app/Constant"
	"project/common/database"
	_http "project/common/http"
)

var headerApi = map[string]string{
	// "Host": "ffapi.jingle.cn",
}

func GetUserInfo(open_id string) (map[string]interface{}, string) {
	cacheKey := "cache:freefire:" + os.Getenv("EVENT_NAME") + ":GetUserInfo:" + open_id
	cacheTTL := 60 * 10
	cacheData, err := database.CacheGet(cacheKey)

	if cacheData == nil {
		cacheMap, ok := cacheData.(map[string]interface{})

		if ok {
			//fmt.Println("ok", cacheMap)
			return cacheMap, ""
		}
	}

	dataset := map[string]string{
		"open_id": open_id,
	}

	response, err := _http.CurlPost(os.Getenv("API_HOST")+"/api/get_account_by_open_id", dataset, true, headerApi)
	//fmt.Println("Response", response.(map[string]interface{}))
	//fmt.Println("err", err)
	if err != nil {
		log.Print(err)
		return nil, Constant.GetWording("timeout")
	}

	data, ok := response.(map[string]interface{})
	if data["error"] == "BR_API_ACCOUNT_NOT_FOUND" {
		log.Print("no Character 1 : " + open_id)
		return nil, Constant.GetWording("nocharacter")
	}
	if !ok {
		return nil, Constant.GetWording("default")
	}

	database.CacheSet(cacheKey, cacheTTL, data)

	return data, ""
}

func GetProfileByAccountID(accountId int) (map[string]interface{}, string) {
	cacheKey := "cache:freefire:" + os.Getenv("EVENT_NAME") + ":GetProfileByAccountID:" + strconv.Itoa(accountId)
	cacheTTL := 60 * 10

	cacheData, err := database.CacheGet(cacheKey)
	if err == nil {
		cacheMap, ok := cacheData.(map[string]interface{})
		if ok {
			return cacheMap, ""
		}
	}

	dataset := map[string]interface{}{
		"account_id": accountId,
	}

	response, err := _http.CurlPost(os.Getenv("API_HOST")+"/apis/get_profile_by_account_id", dataset, true, headerApi)
	if err != nil {
		log.Print(err)
		return nil, Constant.GetWording("timeout")
	}

	if response == nil {
		log.Print("no Character 2")
		return nil, Constant.GetWording("nocharacter")
	}

	data, ok := response.(map[string]interface{})
	if !ok {
		return nil, Constant.GetWording("default")
	}

	database.CacheSet(cacheKey, cacheTTL, data)

	return data, ""
}

func GetAccountWallet(account_id int) (map[string]interface{}, string) {

	dataset := map[string]int{
		"account_id": account_id,
	}

	response, err := _http.CurlPost(os.Getenv("API_HOST")+"/apis/GetAccountWallet", dataset, true, headerApi)

	if err != nil {
		log.Print(err)
		return nil, Constant.GetWording("timeout")
	}

	if response == nil {
		log.Print("1 : " + Constant.GetWording("nowallet"))
		return nil, Constant.GetWording("nowallet")
	}

	result := response.(map[string]interface{})
	if len(result) == 0 {
		log.Print("2 : " + Constant.GetWording("nowallet"))
		return nil, Constant.GetWording("nowallet")
	}

	return result["wallet"].(map[string]interface{}), ""
}

func GetMatchAccountStat(account_id int, start_time int64, end_time int64) ([]map[string]interface{}, string) {

	dataset := map[string]string{
		// "account_id": "231241515",
		"account_id":  strconv.Itoa(account_id),
		"start_time":  strconv.FormatInt(start_time, 10),
		"end_time":    strconv.FormatInt(end_time, 10),
		"lock_region": os.Getenv("LOCK_REGION"),
	}

	response, err := _http.CurlPostInterface(os.Getenv("API_HOST")+"/apis/get_match_account_stats", dataset, true, headerApi)

	// log.Print(err)
	if err != nil {
		log.Print(err)
		return nil, Constant.GetWording("timeout")
	}
	v := reflect.ValueOf(response)
	// result := make(map[string]interface{})
	var result []map[string]interface{}
	for i := 0; i < v.Len(); i++ {
		strct := v.Index(i).Interface()
		newstruct := strct.(map[string]interface{})
		result = append(result, newstruct)
	}

	return result, ""

	//example
	// matchstat,err:=Controller.GetMatchAccountStat(account_id,1550102400,1550188799)
	// // log.Print(matchstat)
	// for _,data := range matchstat {
	//     tmpdata:=data["match_stats"].(map[string]interface{})
	//     if(int(tmpdata["rank"].(float64))<=5){
	//         total_toprank+=1
	//     }
	//     total_survivaltime+=int(tmpdata["survival_time"].(float64))
	// }
	// log.Print("total_survivaltime : " ,total_survivaltime)
	// log.Print("total_toprank : " ,total_toprank)
}

func AddOrDeductGem(account_id int, gems int) (interface{}, string) {

	itemlist := []string{}
	dataset := map[string]interface{}{
		// "account_id": 1231414,
		"account_id": account_id,
		"data": map[string]interface{}{
			"add_item_list": itemlist,
			"coins_delta":   0,
			"gems_delta":    gems,
		},
		"lock_region": os.Getenv("LOCK_REGION"),
	}

	response, err := _http.CurlPost(os.Getenv("API_HOST")+"/apis/ExternalExchangeItems", dataset, true, headerApi)

	if err != nil {
		log.Print(err)
		return nil, Constant.GetWording("timeout")
	}

	if response == nil {
		log.Print(err)
		return nil, Constant.GetWording("processunsuccess")
	}

	return response, ""
}

func SendMails(data map[string]interface{}, countfailsend int) (status string, returnfail int, response interface{}, err error) {
	mailist := []map[string]interface{}{
		map[string]interface{}{
			"title":   data["title"],
			"content": data["content"],
			"source":  1,
			"attachment": map[string]interface{}{
				"rewards": map[string]interface{}{
					"items": data["items"],
					"coins": 0,
					"gems":  0,
				},
			},
		},
	}

	dataset := map[string]interface{}{
		"account_id": data["account_id"],
		"mail_info":  mailist,
	}

	response, err = _http.CurlPost(os.Getenv("API_HOST")+"/api/SendMails", dataset, true, headerApi)

	if err != nil {
		returnfail := countfailsend + 1
		return "fail", returnfail, response, err
	}

	if response == nil {
		returnfail := countfailsend + 1

		return "fail", returnfail, response, errors.New("response is nil")
	}

	// responseMap, ok := response.(map[string]interface{})
	// if !ok {
	// 	return "fail", responseMap, errors.New("response is not map[string]")
	// }

	// if responseMap["status"] == nil {
	// 	return "fail", responseMap, errors.New("no response status")
	// }

	// responseStatus, ok := responseMap["status"].(bool)
	// if !ok {
	// 	return "fail", responseMap, errors.New("response status error")
	// }

	// if responseStatus == false {
	// 	return "fail", responseMap, errors.New("response status false")
	// }

	return "success", 0, response, nil
}

func GetRankingInfoByAccountID(accountId int, seasonId int) (map[string]interface{}, string) {
	cacheKey := "cache:freefire:" + os.Getenv("EVENT_NAME") + ":GetRankingInfoByAccountID:" + strconv.Itoa(accountId) + "_" + strconv.Itoa(seasonId)
	cacheTTL := 60 * 30

	cacheData, err := database.CacheGet(cacheKey)
	if err == nil {
		cacheMap, ok := cacheData.(map[string]interface{})
		if ok {
			return cacheMap, ""
		}
	}

	dataset := map[string]interface{}{
		"account_id": accountId,
		"season_id":  seasonId,
	}

	response, err := _http.CurlPost(os.Getenv("API_HOST")+"/apis/get_ranking_info", dataset, true, headerApi)
	if err != nil {
		log.Print(err)
		return nil, Constant.GetWording("default")
	}

	if response == nil {
		log.Print("no Character")
		return nil, Constant.GetWording("nocharacter")
	}

	data, ok := response.(map[string]interface{})
	if !ok {
		return nil, Constant.GetWording("default")
	}

	database.CacheSet(cacheKey, cacheTTL, data)

	return data, ""
}

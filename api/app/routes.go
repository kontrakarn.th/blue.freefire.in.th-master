package app

import (
	"project/app/Controller/Home"
	"project/app/Controller/Redeem"
	_http "project/common/http"

	// "project/app/Controller/Exchange"
	// "project/app/Controller/Redeem"
	"project/app/Middleware"
)

func GetRoutes() _http.Routes {
	routes := _http.Routes{
		_http.Route{
			Name:        "getHomeInfo",
			Method:      "POST",
			Pattern:     "/api/gethomeinfo",
			HandlerFunc: Home.GetHomeInfo,
		},
		/*_http.Route{
			Name:        "incode",
			Method:      "POST",
			Pattern:     "/api/incode",
			HandlerFunc: Redeem.Incode,
		},*/
		_http.Route{
			Name:        "Redeems",
			Method:      "POST",
			Pattern:     "/api/redeems",
			HandlerFunc: Redeem.Redeems,
		},

		_http.Route{
			Name:        "getHistory",
			Method:      "POST",
			Pattern:     "/api/gethistory",
			HandlerFunc: Home.GetHistory,
		},

		_http.Route{
			Name:        "getOauth",
			Method:      "GET",
			Pattern:     "/api/oauth/user/info/get",
			HandlerFunc: Middleware.GetOauth,
		},
		_http.Route{
			Name:        "getOauthDecrypt",
			Method:      "GET",
			Pattern:     "/api/oauth/user/info/decrypt",
			HandlerFunc: Middleware.GetOauthDecrypt,
		},
	}

	return routes
}

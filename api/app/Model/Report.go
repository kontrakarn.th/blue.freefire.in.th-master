package Model

import (
	"context"
	"log"
	"os"
	"time"

	Constant "project/app/Constant"
	database "project/common/database"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Report struct {
	Id                primitive.ObjectID `bson:"_id" json:"id"`
	Totalredeem       int                `bson:"total_redeem" json:"total_redeem"`
	TotalredeemNormal int                `bson:"total_redeem_normal" json:"total_redeem_normal"`
	TotalredeemLegend int                `bson:"total_redeem_legendary" json:"total_redeem_legendary"`
	Uniqueredeem      int                `bson:"unique_redeem" json:"unique_redeem"`
	ReportDate        string             `bson:"report_date" json:"report_date"`
}
type Returnreport struct {
	TotalRedeem       int
	TotalNormalRedeem int
	TotalLegendRedeem int
}

func ReportIncrement(field string, amount int) {
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {

		log.Print("cant connect db")
		// return data, Constant.GetWording("cantconnectdb"), false
	}
	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME") + "_report")
	loc, _ := time.LoadLocation(os.Getenv("TZ"))
	n := time.Now().In(loc)
	n_string := n.Format("2006-01-02")

	opts := options.Count().SetMaxTime(2 * time.Second)
	numdate, err_numdate := col.CountDocuments(context.TODO(), bson.M{"report_date": n_string}, opts)

	if err_numdate != nil {
		log.Print(err_numdate)
	}

	if numdate == 0 {
		reportins := &Report{
			Id:         primitive.NewObjectID(),
			ReportDate: n_string,
		}

		// err_ins := col.Insert(reportins)
		_, err_ins := col.InsertOne(context.TODO(), reportins)
		// log.Print(res)
		if err_ins != nil {
			log.Print(err_ins)
		}
	}

	opts_2 := options.FindOneAndUpdate().SetUpsert(true)
	filter := bson.M{"report_date": n_string}
	update := bson.M{"$inc": bson.M{field: amount}}
	var updatedDocument bson.M
	err := col.FindOneAndUpdate(context.TODO(), filter, update, opts_2).Decode(&updatedDocument)
	if err != nil {
		log.Print(err)
	}

	return
}
func GetReportfunc() ([]Report, string, bool) {
	var data []Report
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		return data, Constant.GetWording("cantconnectdb"), false
	}

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME") + "_report")
	opts := options.Find().SetSort(bson.D{{"_id", 1}})
	cursor, err := col.Find(context.TODO(), bson.M{}, opts)
	if err != nil {
		log.Print("error|GetAllItem|", err)
		return data, Constant.GetWording("default") + " (11)", false
	}

	if err = cursor.All(context.TODO(), &data); err != nil {
		log.Print(err)
		return data, Constant.GetWording("default") + " (22)", false
	}

	return data, Constant.GetWording("success"), true
}

func GetAlleportfunc() (Returnreport, string, bool) {
	var data []Report
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		Returndata := Returnreport{TotalRedeem: 0, TotalNormalRedeem: 0, TotalLegendRedeem: 0}
		return Returndata, Constant.GetWording("cantconnectdb"), false
	}

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME") + "_report")
	opts := options.Find().SetSort(bson.D{{"_id", 1}})
	cursor, err := col.Find(context.TODO(), bson.M{}, opts)
	if err != nil {
		log.Print("error|GetAllItem|", err)
		Returndata := Returnreport{TotalRedeem: 0, TotalNormalRedeem: 0, TotalLegendRedeem: 0}
		return Returndata, Constant.GetWording("default") + " (11)", false
	}

	if err = cursor.All(context.TODO(), &data); err != nil {
		log.Print(err)
		Returndata := Returnreport{TotalRedeem: 0, TotalNormalRedeem: 0, TotalLegendRedeem: 0}
		return Returndata, Constant.GetWording("default") + " (22)", false
	}
	totalredeem := 0
	totalredeemnormal := 0
	totalredeemlegend := 0

	for _, num := range data {
		totalredeem += num.Totalredeem
		totalredeemnormal += num.TotalredeemNormal
		totalredeemlegend += num.TotalredeemLegend
	}

	Returndata := Returnreport{TotalRedeem: totalredeem, TotalNormalRedeem: totalredeemnormal, TotalLegendRedeem: totalredeemlegend}

	return Returndata, Constant.GetWording("success"), true
}

package Model

import (
	"context"
	"log"
	"os"

	// "time"
	Constant "project/app/Constant"
	database "project/common/database"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Items struct {
	//Id     primitive.ObjectID `bson:"_id" json:"-"`
	No     int        `bson:"no" json:"no"`
	Image  int        `bson:"image" json:"image"`
	Image2 int        `bson:"image2" json:"image2"`
	Items  []ItemList `bson:"items" json:"items"`
}

type ItemList struct {
	Type   string `bson:"type" json:"-"`
	Id     int    `bson:"id" json:"-"`
	Name   string `bson:"name" json:"name"`
	NameEn string `bson:"name_en" json:"-"`
	Cnt    int    `bson:"cnt" json:"-"`
}

func GetOneitem() ([]Items, string, bool) {
	var data Items
	var dataret []Items
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		return dataret, Constant.GetWording("cantconnectdb"), false
	}
	// defer mongo.Close()
	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME") + "_items")
	err := col.FindOne(context.Background(), bson.M{"no": 1}).Decode(&data)
	if err != nil {
		if err.Error() != Constant.GetWording("mongo_data_not_found") {
			log.Print("error while getting user event info: ", err)
			return dataret, Constant.GetWording("default"), false
		}
		return dataret, Constant.GetWording("nodata"), false
	}
	dataret = append(dataret, data)
	return dataret, Constant.GetWording("success"), true
}
func GetTwoitem() ([]Items, string, bool) {
	var data []Items
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		return data, Constant.GetWording("cantconnectdb"), false
	}

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME") + "_items")
	opts := options.Find().SetSort(bson.D{{"no", 1}})
	cursor, err := col.Find(context.TODO(), bson.M{}, opts)
	if err != nil {
		log.Print("error|GetAllItem|", err)
		return data, Constant.GetWording("default") + " (11)", false
	}

	if err = cursor.All(context.TODO(), &data); err != nil {
		log.Print(err)
		return data, Constant.GetWording("default") + " (22)", false
	}

	return data, Constant.GetWording("success"), true
}
func GetItemByNo(n int) (Items, string, bool) {
	var data Items
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		return data, Constant.GetWording("cantconnectdb"), false
	}

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME") + "_items")
	err := col.FindOne(context.Background(), bson.M{"no": n}).Decode(&data)

	if err != nil {
		if err.Error() != Constant.GetWording("mongo_data_not_found") {
			log.Print("error while getting item no data: ", err)
		}
		return data, Constant.GetWording("nodata"), false
	}
	return data, Constant.GetWording("success"), true
}

func GetAllItem() ([]Items, string, bool) {
	var data []Items
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		return data, Constant.GetWording("cantconnectdb"), false
	}

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME") + "_items")
	opts := options.Find().SetSort(bson.D{{"no", 1}})
	cursor, err := col.Find(context.TODO(), bson.M{}, opts)
	if err != nil {
		log.Print("error|GetAllItem|", err)
		return data, Constant.GetWording("default") + " (11)", false
	}

	if err = cursor.All(context.TODO(), &data); err != nil {
		log.Print(err)
		return data, Constant.GetWording("default") + " (22)", false
	}

	return data, Constant.GetWording("success"), true
}

func UpdateItem(colQuerier bson.M, change bson.M) (Items, string, bool) {
	var data Items
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {

		log.Print("cant connect db")
		return data, Constant.GetWording("cantconnectdb"), false
	}
	// defer mongo.Close()
	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME") + "_items")
	_, err := col.UpdateOne(context.TODO(), colQuerier, change)
	if err != nil {
		log.Print("error while update item info: ", err)
		return data, Constant.GetWording("nodata"), false
	}

	return data, Constant.GetWording("success"), true
}

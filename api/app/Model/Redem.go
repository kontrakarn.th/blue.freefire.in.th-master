package Model

import (
	"context"
	"log"
	"os"
	Constant "project/app/Constant"
	database "project/common/database"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Redeem struct {
	Id          primitive.ObjectID `bson:"_id" json:"id"`
	AccountId   int                `bson:"account_id" json:"account_id"`
	RedeemtDate string             `bson:"redeem_date" json:"redeem_date"`
	CreatedAt   time.Time          `bson:"created_at" json:"created_at"`
	UpdatedAt   time.Time          `bson:"updated_at" json:"updated_at"`
}

func ReportUniqueRedeem(account_id int) {
	var data Visit
	mongo := database.GetNewMongoSession("mongo")
	if mongo == nil {
		log.Print("cant connect db")
		// return data, Constant.GetWording("cantconnectdb"), false
	}

	loc, _ := time.LoadLocation(os.Getenv("TZ"))
	n := time.Now().In(loc)
	n_string := n.Format("2006-01-02")

	col := mongo.Database(os.Getenv("MONGO_DATABASE")).Collection(os.Getenv("EVENT_NAME") + "_unique_redeem_daily")
	err := col.FindOne(context.Background(), bson.M{"redeem_date": n_string, "account_id": account_id}).Decode(&data)
	if err != nil {
		if err.Error() != Constant.GetWording("mongo_data_not_found") {
			log.Print("error while getting visit event info: ", err)
		} else {
			reportins := &Redeem{
				Id:          primitive.NewObjectID(),
				AccountId:   account_id,
				RedeemtDate: n_string,
				CreatedAt:   n,
				UpdatedAt:   n,
			}

			_, err_ins := col.InsertOne(context.TODO(), reportins)
			// log.Print(res)
			// err_ins := col.Insert(reportins)
			if err_ins != nil {
				log.Print(err_ins)
			}
			ReportIncrement("unique_redeem", 1)
		}
	}

	return
}

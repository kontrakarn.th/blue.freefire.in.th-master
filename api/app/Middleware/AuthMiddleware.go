package Middleware

import (
	// "encoding/json"
	// "io/ioutil"

	"crypto/aes"
	"crypto/cipher"
	"encoding/hex"
	"fmt"
	"log"
	"net/http"
	"os"
	Constant "project/app/Constant"
	database "project/common/database"
	_http "project/common/http"

	"github.com/dgrijalva/jwt-go"
	pkcs7 "github.com/mergermarket/go-pkcs7"
	// common "project/common"
	// utils "project/common/utils"
	// "math/rand"
	// "time"
)

func GetOauth(w http.ResponseWriter, r *http.Request) {

	keys, ok := r.URL.Query()["access_token"]
	if !ok || len(keys[0]) < 1 {
		log.Print("param 'access_token' is missing")
		Constant.ResponseMessage(w, nil, "Error(1)", false)
		return
	}

	token := keys[0]
	result, err := database.CacheRemember(
		"Freefire:"+os.Getenv("EVENT_NAME")+":get_oauth:token:"+token,
		300, // 5 mins
		func() interface{} {
			r, err := _http.CurlGet(os.Getenv("OAUTH_HOST") + "/oauth/user/info/get?access_token=" + token)

			if err != nil {
				log.Print("error while getting oauth: ", err)
				return nil
			}
			return r
		},
	)
	if err == nil && result != nil {

		response := result.(map[string]interface{})
		// fmt.Println("",respoxnse)
		if response["uid"] == nil {
			log.Print("error while getting oauth: invalid response")
		} else {
			// do jwt
			jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
				"uid":      response["uid"],
				"open_id":  response["open_id"],
				"gender":   response["gender"],
				"platform": response["platform"],
				"nickname": response["nickname"],
				"icon":     response["icon"],
			})

			// sign
			jwtTokenString, err := jwtToken.SignedString([]byte(os.Getenv("APP_SECRET")))
			if err != nil {
				log.Print("error while getting oauth: ", err)
				Constant.ResponseMessage(w, nil, "Error(2)", false)
				return
			}

			data := map[string]interface{}{
				"token": jwtTokenString,
			}

			Constant.ResponseMessage(w, data, "Success!", true)

			return
		}
	} else {
		log.Print("error while getting oauth: response is nil")
	}

	Constant.ResponseMessage(w, nil, "Error(3)", false)
}

func GetOauthDecrypt(w http.ResponseWriter, r *http.Request) {

	keys, ok := r.URL.Query()["access_token"]

	if !ok || len(keys[0]) < 1 {
		log.Print("param 'access_token' is missing")
		Constant.ResponseMessage(w, nil, "Error(1)", false)
		return
	}

	encrypt_token := keys[0]

	token, err := Decrypt(encrypt_token)
	if err != nil {
		log.Print("Failed to decrypt: %s - %s", keys[0], err.Error())
	}

	result, err := database.CacheRemember(
		"Freefire:"+os.Getenv("EVENT_NAME")+":get_oauth:token:"+token,
		300, // 5 mins
		func() interface{} {
			r, err := _http.CurlGet(os.Getenv("OAUTH_HOST") + "/oauth/user/info/get?access_token=" + token)

			if err != nil {
				log.Print("error while getting oauth: ", err)
				return nil
			}
			return r
		},
	)
	if err == nil && result != nil {
		response := result.(map[string]interface{})
		if response["uid"] == nil {
			log.Print("error while getting oauth: invalid response")
		} else {
			// do jwt
			jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
				"uid":      response["uid"],
				"open_id":  response["open_id"],
				"gender":   response["gender"],
				"platform": response["platform"],
				"nickname": response["nickname"],
				"icon":     response["icon"],
			})

			// sign
			jwtTokenString, err := jwtToken.SignedString([]byte(os.Getenv("APP_SECRET")))
			if err != nil {
				log.Print("error while getting oauth: ", err)
				Constant.ResponseMessage(w, nil, "Error(2)", false)
				return
			}

			data := map[string]interface{}{
				"token": jwtTokenString,
			}

			Constant.ResponseMessage(w, data, "Success!", true)

			return
		}
	} else {
		log.Print("error while getting oauth: response is nil")
	}

	Constant.ResponseMessage(w, nil, "Error(3)", false)
}

func Encrypt(unencrypted string) (string, error) {
	key := []byte(os.Getenv("ACCESS_TOKEN_KEY"))
	plainText := []byte(unencrypted)
	plainText, err := pkcs7.Pad(plainText, aes.BlockSize)
	if err != nil {
		return "", fmt.Errorf(`plainText: "%s" has error`, plainText)
	}

	if len(plainText)%aes.BlockSize != 0 {
		err := fmt.Errorf(`plainText: "%s" has the wrong block size`, plainText)
		return "", err
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	cipherText := make([]byte, aes.BlockSize+len(plainText))
	iv := []byte(os.Getenv("ACCESS_TOKEN_IV"))

	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(cipherText[aes.BlockSize:], plainText)

	return fmt.Sprintf("%x", cipherText[aes.BlockSize:]), nil
}

// Decrypt decrypts cipher text string into plain text string
func Decrypt(encrypted string) (string, error) {
	// key := []byte(os.Getenv("ACCESS_TOKEN_KEY"))
	cipherText, _ := hex.DecodeString(encrypted)

	block, err := aes.NewCipher([]byte("?\xe8p~\xe4\xb0\x9f2\xf4,>\xbf0m\x9f\xe4i\xd9\xac\x10\x98\xfe\xfd\xbfE\x87\x14\xfe\xf7\xf6\xc1\xbb"))
	if err != nil {
		panic(err)
	}

	if len(cipherText) < aes.BlockSize {
		panic("cipherText too short")
	}

	iv := []byte("\xe6\xf3\xf3\xb5\x02_Y\xd1\xec\xaf\x8c]\xe1\xc1K7")

	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(cipherText, cipherText)
	cipherText, _ = pkcs7.Unpad(cipherText, aes.BlockSize)

	return fmt.Sprintf("%s", cipherText), nil
}

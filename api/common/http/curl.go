package http

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
	// "os"
)

/**
 * Make HTTP GET request to specified URL
 *
 * @param url string The target URL
 *
 * @return interface{}, error
 */
func CurlGet(url string) (interface{}, error) {
	// make request
	req, err := http.NewRequest("GET", url, nil)
	client := &http.Client{
		Timeout: time.Duration(5 * time.Second),
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	var r map[string]interface{}
	if err := json.Unmarshal(body, &r); err == nil {
		return r, nil
	}
	return nil, err
}

/**
 * Make HTTP GET request to specified URL
 *
 * @param url string The target URL
 *
 * @return interface{}, error
 */
// func CurlGetWithHeader(url string)  (interface{}, error) {
// 	// make request
// 	req, err := http.NewRequest("GET", url, nil)
// 	client := &http.Client{
// 		Timeout: time.Duration(5 * time.Second),
// 	}
// 	req.Header.Set("Authorization", os.Getenv("GAME_API_AUTHORIZATION"))
// 	resp, err := client.Do(req)
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer resp.Body.Close()
// 	body, err := ioutil.ReadAll(resp.Body)
//
// 	var r map[string]interface{}
// 	if err := json.Unmarshal(body, &r); err == nil {
// 		return r, nil;
// 	}
// 	return nil, err
// }

func CurlGetWithHeader(url string, headers map[string]string) (interface{}, error) {
	// make request
	req, err := http.NewRequest("GET", url, nil)
	client := &http.Client{
		Timeout: time.Duration(2 * time.Second),
	}
	if headers != nil {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	var r map[string]interface{}
	if err := json.Unmarshal(body, &r); err == nil {
		return r, nil
	}
	return nil, err
}

/**
 * Make HTTP POST request to specified URL
 *
 * @param url string The target URL
 * @param data interface{} For urlencoded, data MUST be map[string]string
 * @param isJson bool TRUE if body is json, FALSE otherwise
 *
 * @return interface{}, error
 */
func CurlPost(url string, data interface{}, isJson bool, headers map[string]string) (interface{}, error) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	// build payload string
	var payload string
	if data == nil {
		// empty payload
		payload = ""
	} else if isJson {
		// application/json
		b, err := json.Marshal(data)
		if err != nil {
			return nil, err
		}
		payload = string(b)
	} else {
		// application/x-www-form-urlencoded
		b := data.(map[string]string)
		var pre = ""
		for k, v := range b {
			payload += pre + k + "=" + v
			pre = "&"
		}
	}
	// make request
	req, err := http.NewRequest("POST", url, bytes.NewBuffer([]byte(payload)))
	if isJson {
		req.Header.Set("Content-Type", "application/json")
	} else {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}

	if headers != nil {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}
	client := &http.Client{
		Timeout:   time.Duration(5 * time.Second),
		Transport: tr,
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	var r map[string]interface{}
	if err := json.Unmarshal(body, &r); err == nil {
		return r, nil
	}
	return nil, err
}

func CurlPostInterface(url string, data interface{}, isJson bool, headers map[string]string) (interface{}, error) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	// build payload string
	var payload string
	if data == nil {
		// empty payload
		payload = ""
	} else if isJson {
		// application/json
		b, err := json.Marshal(data)
		if err != nil {
			return nil, err
		}
		payload = string(b)
	} else {
		// application/x-www-form-urlencoded
		b := data.(map[string]string)
		var pre = ""
		for k, v := range b {
			payload += pre + k + "=" + v
			pre = "&"
		}
	}

	// make request
	req, err := http.NewRequest("POST", url, bytes.NewBuffer([]byte(payload)))
	if isJson {
		req.Header.Set("Content-Type", "application/json")
	} else {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}

	if headers != nil {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}
	client := &http.Client{
		Timeout:   time.Duration(5 * time.Second),
		Transport: tr,
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	var r interface{}
	if err := json.Unmarshal(body, &r); err == nil {
		return r, nil
	}
	return nil, err
}

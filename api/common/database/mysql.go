package database

//
// @author ggadv2
//

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func SqlInsert(con *sql.DB, query string, args ...interface{}) (int, error) {
	stmt, err := con.Prepare(query)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()

	var res sql.Result
	res, err = stmt.Exec(args...)
	if err != nil {
		return 0, err
	}

	lastId, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}
	return int(lastId), nil
}

func SqlUpdate(con *sql.DB, query string, args ...interface{}) (int, error) {
	stmt, err := con.Prepare(query)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()

	var res sql.Result
	res, err = stmt.Exec(args...)
	if err != nil {
		return 0, err
	}

	rowCnt, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	return int(rowCnt), nil
}

func SqlCount(con *sql.DB, query string, args ...interface{}) (int, error) {
	var count int
	stmt, err := con.Prepare(query)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()

	err = stmt.QueryRow(args...).Scan(
		&count,
	)
	if err != nil && err != sql.ErrNoRows {
		return 0, err
	}
	return count, nil
}
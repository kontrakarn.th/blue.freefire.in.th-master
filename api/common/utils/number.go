package utils

import (
	"math/rand"
	"time"
)

/**
 * Get a random number between specified min and max value
 * 
 * @param min int Minimum value
 * @param max int Maximum value
 *
 * @return int
 */
func Random(min int, max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max - min) + min
}
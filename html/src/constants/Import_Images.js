const BgBlue = 'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/bg_blue.png';
const BgLogin =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/bg_login.png';
const footerRightBlue =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/footer_right_blue.png';
const StatusCollection =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/status_collection.png';
const Submarine =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/submarine.png';
const SubmarineTop =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/submarine_top.png';
const CircleMain =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/circle_main.png';
const Circle = 'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/circle.png';
const SubmarineCircle =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/submarine_circle.png';
const Hologram =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/hologram.png';
const HologramItem1 =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/hologram_item1.png';
const HologramItem2 =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/hologram_item2.png';
const HologramItem3 =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/hologram_item3.png';
const Watersurface =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/watersurface.png';
const CountdownBar =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/countdown_bar.png';
const UidCover =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/uid_cover.png';
const HistoryBtn =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/history_btn.png';
const HowToBtn =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/how_to_btn.png';
const LogOutBtn =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/log_out_btn.png';
const LogInBtn =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/log_in_btn.png';
const ConfirmBtn =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/confirm_btn.png';
const BlueBox =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/blue_box.png';
const WhiteBox =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/white_box.png';
const BtnFb = 'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/btn_fb.png';
const BtnGg = 'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/btn_gg.png';
const BtnVk = 'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/btn_vk.png';
const ContentItemLeft =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/content_item_left.png';
const ContentItemRight =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/content_item_right.png';
const BgModal =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/bg_modal.png';
const xClose = 'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/x.png';
const BgModalReward =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/bg_modal_reward.png';
const CongratsText =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/congrats_text.png';
const Screen = 'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/screen.png';
const ArmRight =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/arm_right.png';
const ArmLeft =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/arm_left.png';
const BArmRight =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/b_arm_right.png';
const BArmLeft =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/b_arm_left.png';
const BgUnderWater =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/bg_under_water.png';
const BlackBoxLeft =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/black_box_left.png';
const BlackBoxRight =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/black_box_right.png';
const SkipAnimationText =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/skip_animation_text.png';
const BtnTac = 'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/btn_tac.png';
const BLueBoxUnderwater =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/blue_box_underwater.png';
const WhiteBoxUnderwater =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/blue/white_box_underwater.png';

const bg_blue = BgBlue;
const bg_login = BgLogin;
const footer_left =
    'https://cdngarenanow-a.akamaihd.net/webth/ff/events/garnier2020/images/footer_left.png';
const footer_right = footerRightBlue;
const status_collection = StatusCollection;
const submarine = Submarine;
const submarine_top = SubmarineTop;
const circle_main = CircleMain;
const circle = Circle;
const submarine_circle = SubmarineCircle;
const hologram = Hologram;
const hologram_item1 = HologramItem1;
const hologram_item2 = HologramItem2;
const hologram_item3 = HologramItem3;
const watersurface = Watersurface;
const countdown_bar = CountdownBar;
const uid_cover = UidCover;
const history_btn = HistoryBtn;
const how_to_btn = HowToBtn;
const log_out_btn = LogOutBtn;
const log_in_btn = LogInBtn;
const confirm_btn = ConfirmBtn;
const btn_tac = BtnTac;
const blue_box = BlueBox;
const white_box = WhiteBox;
const btn_fb = BtnFb;
const btn_gg = BtnGg;
const btn_vk = BtnVk;
const content_item_left = ContentItemLeft;
const content_item_right = ContentItemRight;
const bg_modal = BgModal;
const x = xClose;
const bg_modal_reward = BgModalReward;
const congrats_text = CongratsText;
const screen = Screen;
const arm_right = ArmRight;
const arm_left = ArmLeft;
const b_arm_right = BArmRight;
const b_arm_left = BArmLeft;
const bg_under_water = BgUnderWater;
const black_box_left = BlackBoxLeft;
const black_box_right = BlackBoxRight;
const skip_animation_text = SkipAnimationText;
const blue_box_underwater = BLueBoxUnderwater;
const white_box_underwater = WhiteBoxUnderwater;

export const imageList = {
    bg_blue,
    bg_login,
    bg_modal,
    btn_fb,
    btn_gg,
    btn_vk,
    btn_tac,
    content_item_left,
    content_item_right,
    status_collection,
    footer_right,
    footer_left,
    submarine,
    submarine_top,
    circle_main,
    circle,
    submarine_circle,
    hologram,
    hologram_item1,
    hologram_item2,
    hologram_item3,
    watersurface,
    countdown_bar,
    uid_cover,
    history_btn,
    how_to_btn,
    log_out_btn,
    log_in_btn,
    confirm_btn,
    blue_box,
    white_box,
    x,
    bg_modal_reward,
    congrats_text,
    screen,
    arm_right,
    arm_left,
    b_arm_right,
    b_arm_left,
    bg_under_water,
    black_box_left,
    black_box_right,
    skip_animation_text,
    blue_box_underwater,
    white_box_underwater,
};

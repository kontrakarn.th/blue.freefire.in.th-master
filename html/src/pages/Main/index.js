import React from 'react';
import { connect } from 'react-redux';
import { imageList } from './../../constants/Import_Images';
import { setValues } from '../../store/redux';
import styled from 'styled-components';
//features
import LoginPage from '../../features/Login';
//component
import {
    HistoryModal,
    H2PModal,
    LoadingModal,
    RewardModal,
    MessageModal,
    PolicyModal,
    WarningModal,
    PolicyAfLoginModal,
} from '../../features/modal';
import {
    DecorContainerLeft,
    DecorContainerRight,
    Footer,
    ContentCodeContainer,
} from '../../components';

const Main = (props) => {
    return (
        <Layout>
            <RatioPreserver>
                {props.jwtToken && props.jwtToken !== '' ? (
                    <>
                        <DecorContainerLeft />
                        <ContentCodeContainer />
                        <DecorContainerRight />
                        <Footer />
                    </>
                ) : (
                    <LoginPage />
                )}
            </RatioPreserver>
            <HistoryModal />
            <H2PModal />
            <LoadingModal />
            <RewardModal />
            <MessageModal />
            <PolicyModal />
            <WarningModal />
            <PolicyAfLoginModal />
        </Layout>
    );
};

const mapStateToProps = (state) => ({ ...state.Main });

const mapDispatchToProps = { setValues };

export default connect(mapStateToProps, mapDispatchToProps)(Main);

const Layout = styled.div`
    background: top center no-repeat url(${imageList['bg_blue']});
    background-size: cover;
    width: 100vw;
    height: 100vh;
    background-size: contain;
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    background-color: #000;
`;

const RatioPreserver = styled.div`
    position: absolute;
    display: inline-block;
    width: 100vw;
    height: 100vh;
    max-width: calc(100vh / 9 * 16);
    max-height: calc(100vw / 16 * 9);
    top: 0;
    margin: 0 auto;
    overflow: hidden;
`;

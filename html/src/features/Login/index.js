import React from 'react';
import { imageList } from '../../constants/Import_Images';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { setValues } from '../../store/redux';
const LoginPage = (props) => {
    return (
        <LoginWrapper>
            <LoginContent>
                <BtnBox>
                    <a
                        href={
                            props.loginUrl &&
                            props.loginUrl.replace('{{platform}}', 3)
                        }
                        className='loginbtns'
                    >
                        <img src={imageList['btn_fb']} alt='' />
                    </a>
                    <a
                        href={
                            props.loginUrl &&
                            props.loginUrl.replace('{{platform}}', 8)
                        }
                        className='loginbtns'
                    >
                        <img src={imageList['btn_gg']} alt='' />
                    </a>
                    <a
                        href={
                            props.loginUrl &&
                            props.loginUrl.replace('{{platform}}', 5)
                        }
                        className='loginbtns'
                    >
                        <img src={imageList['btn_vk']} alt='' />
                    </a>
                </BtnBox>
            </LoginContent>
        </LoginWrapper>
    );
};

const mapStateToProps = (state) => ({ ...state.Main });

const mapDispatchToProps = { setValues };

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);

const LoginWrapper = styled.div`
    display: block;
    margin: 0 auto;
    width: 100%;
    height: 100%;
    position: relative;
    box-sizing: border-box;
    background: top center no-repeat url(${imageList['bg_login']});
    background-size: contain;
`;

const LoginContent = styled.div`
    display: block;
    margin: 0 auto;
    width: 100%;
    position: absolute;
    top: 34%;
    left: 0;
    height: 54%;
    .title {
        color: #fff;
        font-size: 1em;
        text-align: center;
        margin: 0 auto;
        line-height: 1;
    }
`;

const BtnBox = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    // width: 56%;
    width: 27%;
    margin: 3% auto 0;
    height: 75%;
    flex-flow: column;
    .loginbtns {
        cursor: pointer;
        width: 100%;
        > img {
            display: block;
            margin: 0 auto;
        }
    }
`;

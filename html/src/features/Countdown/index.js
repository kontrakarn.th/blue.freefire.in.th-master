import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

export default (props) => {
    const [seconds, setSeconds] = useState('00');
    const [minutes, setMinutes] = useState('00');
    const [hours, setHours] = useState('00');
    const [days, setDays] = useState('0');

    const getTimeUntil = (deadline) => {
        let arr = deadline.split(/[- :]/),
            date_dead = new Date(
                arr[0],
                arr[1] - 1,
                arr[2],
                arr[3],
                arr[4],
                arr[5]
            );

        const time = date_dead - Date.parse(new Date());
        // console.log("time",time)
        if (time < 0) {
            const seconds = '00';
            const minutes = '00';
            const hours = '00';
            const days = '0';

            setSeconds(seconds);
            setMinutes(minutes);
            setHours(hours);
            setDays(days);
        } else {
            const seconds = ('0' + Math.floor((time / 1000) % 60)).slice(-2);
            const minutes = ('0' + Math.floor((time / 1000 / 60) % 60)).slice(
                -2
            );
            const hours = (
                '0' + Math.floor((time / (1000 * 60 * 60)) % 24)
            ).slice(-2);
            const days = ('' + Math.floor(time / (1000 * 60 * 60 * 24))).slice(
                -3
            );

            setSeconds(seconds);
            setMinutes(minutes);
            setHours(hours);
            setDays(days);
        }
    };

    useEffect(() => {
        setInterval(() => getTimeUntil(props.deadline), 1000);
    });
    return (
        <Timer key={props.name}>
            <div className='timer__unit--container'>
                เหลือระยะเวลากิจกรรม{' '}
                <span className='timer__unit--color'>>></span>{' '}
                <div className='timer__unit--color'>
                    {days}
                    <span>day</span>
                </div>{' '}
                :{' '}
                <div className='timer__unit--color'>
                    {hours}
                    <span>hrs</span>
                </div>{' '}
                :
                <div className='timer__unit--color'>
                    {minutes}
                    <span>min</span>
                </div>{' '}
                :{' '}
                <div className='timer__unit--color'>
                    {seconds}
                    <span>sec</span>
                </div>
            </div>
        </Timer>
    );
};

const Timer = styled.div`
    position: absolute;
    align-items: center;
    display: flex;
    width: 100%;
    height: 100%;
    justify-content: center;

    .timer__unit--container {
        display: flex;
        flex-direction: row;
        width: 70%;
        justify-content: space-evenly;
        font-size: 1em;
        pointer-events: none;

        @media (min-width: 1025px) and (max-width: 1120px) {
            font-size: 0.7em;
        }

        @media (max-width: 1024px) {
            font-size: 0.8em;
        }

        .timer__unit--color {
            color: #00ffff;
        }

        div {
            display: flex;
            flex-direction: column;
            align-items: center;
            line-height: 0.9;
            align-self: center;
            font-size: 0.8em;
            span {
                text-transform: uppercase;
                font-size: 0.4em;
                color: #fff;
            }
        }
    }
`;

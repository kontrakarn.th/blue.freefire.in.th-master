import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import { imageList } from '../../constants/Import_Images';

const HistoryModal = (props) => {
    return (
        <ModalCore
            modal_name='history'
            allowOutsideClicked={() => props.setValues({ modal_open: '' })}
            close_btn={true}
        >
            <ModalContent>
                <TitleBox>ประวัติรางวัล</TitleBox>
                <div className='content h2p'>
                    {props.history &&
                        props.history.length > 0 &&
                        props.history.map((item, key) => {
                            return (
                                <ItemHistory key={`historylist_${key + 1}`}>
                                    <div className='history__content'>
                                        <div>
                                            {item.item_list.map((item, key) => {
                                                return (
                                                    <img
                                                        key={`historyimg_${
                                                            key + 1
                                                        }`}
                                                        src={
                                                            item.no === 1
                                                                ? imageList[
                                                                      'white_box'
                                                                  ]
                                                                : imageList[
                                                                      'blue_box'
                                                                  ]
                                                        }
                                                        alt={item.no}
                                                    />
                                                );
                                            })}
                                        </div>
                                    </div>
                                    <div className='history__content'>
                                        <span className='titiebox'>
                                            <p>กล่อง </p>
                                            {item.item_list.map((item, key) => {
                                                return (
                                                    <React.Fragment>
                                                        <div
                                                            className='text_name'
                                                            key={`historydesc_${
                                                                key + 1
                                                            }`}
                                                        >
                                                            {item.items &&
                                                                item.items.map(
                                                                    (
                                                                        name,
                                                                        key
                                                                    ) =>
                                                                        name.name
                                                                )}
                                                        </div>
                                                    </React.Fragment>
                                                );
                                            })}
                                        </span>
                                        <span className='codebox'>
                                            รหัส : {item.code}
                                        </span>
                                        <span>
                                            วันที่ได้ : {item.created_at}
                                        </span>
                                    </div>
                                </ItemHistory>
                            );
                        })}
                </div>
            </ModalContent>
        </ModalCore>
    );
};

const mapStateToProps = (state) => ({ ...state.Main });
const mapDispatchToProps = { setValues };

export default connect(mapStateToProps, mapDispatchToProps)(HistoryModal);

const ModalContent = styled.div`
    position: relative;
    width: calc(100vw / 1920 * 1387);
    height: calc(100vh / 1080 * 733);
    background: top center no-repeat url(${imageList['bg_modal']});
    background-size: 100% 100%;
    color: #ffffff;
    word-break: break-word;
    padding: 4%;
    box-sizing: border-box;
    .text {
        &--green {
            color: #1b361e;
        }
        &--blue {
            color: #0b2e47;
        }
        &--purple {
            color: #460b3c;
        }
        &--gold {
            color: #bc4c00;
        }
    }
    .inner_title {
        width: 100%;
        height: 20%;
        > img {
            display: block;
            margin: 0 auto;
        }
    }
    .content {
        width: 92%;
        height: 76%;
        padding: 0% 0;
        margin: 2% auto 0;
        text-align: center;
        color: #000000;
        overflow: auto;

        .historylist {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin: 1% auto;
            /* height: 30%; */
            width: 80%;
            margin: 2% auto 0;
            .itembox {
                width: 32%;
                height: 100%;
                border-right: 0.3vh solid #00ac4f;
                > img {
                    display: block;
                    margin: 0 auto;
                }
            }
        }
        &::-webkit-scrollbar {
            width: 15px;
            background: #2c5a84;
        }
        &::-webkit-scrollbar-thumb {
            background: #fff;
        }
        &.h2p {
            text-align: left;
            color: #ffffff;
            @media (max-width: 1024px) {
                font-size: 0.7em;
            }
            h4 {
                margin: 0 0 1em 0;
                text-align: center;
            }
            ul {
                width: 96%;
                height: 100%;
                overflow: auto;
                margin: 0;
                padding-left: 4%;
                border-radius: 5px;
                padding-right: 3%;
                font-size: 1em;
                .info {
                    text-align: left;
                    font-size: 1em;
                    line-height: 1;
                    width: 60%;
                    margin-left: 5%;
                    &__name {
                        color: #00ac4f;
                    }
                }
                &::-webkit-scrollbar {
                    width: 15px;
                    background: #070707;
                    @media (max-width: 1024px) {
                        width: 10px;
                    }
                }
                &::-webkit-scrollbar-thumb {
                    background: #00ac4f;
                }
                > li {
                    width: 100%;
                    text-align: left;
                    line-height: 1.3;
                }
            }
        }
    }
`;
const TitleBox = styled.div`
    display: block;
    margin: 0 auto;
    font-size: 2em;
    color: #ffffff;
    text-align: center;
    height: 15%;
    line-height: 1;
    > img {
        display: block;
        margin: 0 auto;
    }
`;
const ItemHistory = styled.div`
    display: flex;
    margin-bottom: 5%;
    line-height: 1.5;

    .history__content {
        .titiebox {
            display: flex;
            font-size: 0.8em;

            .text_name {
                &:not(:last-child):after {
                    content: ' + ';
                }

                &:last-child {
                    margin-left: 1%;
                }
            }

            > p {
                margin: 0 1% 0 0;
            }
        }

        .codebox {
            font-size: 0.8em;
        }

        &:nth-child(1) {
            flex: 1.1;
            border-right: 2.5px solid #fff;
            position: relative;
            display: flex;
            justify-content: center;
            align-content: center;
            align-items: center;

            img {
                width: 9vmax;
                height: auto;
                position: relative;
                padding: 0 6%;
            }
        }
        &:nth-child(2) {
            flex: 2;
            flex-direction: column;
            display: flex;
            padding: 0.8% 3%;
        }
    }
`;

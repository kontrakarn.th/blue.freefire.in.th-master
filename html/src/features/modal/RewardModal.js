import React from 'react';
import { connect } from 'react-redux';
import styled, { keyframes } from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import { imageList } from '../../constants/Import_Images';

const CPN = (props) => {
    return (
        <ModalCore
            modal_name='reward'
            allowOutsideClicked={true}
            close_btn={true}
            full_modal={true}
        >
            <ModalMessageContent>
                <div className='content_items'>
                    <img src={imageList['congrats_text']} alt='' />
                </div>
                <div className='content_items'>
                    {props.item_get &&
                        props.item_get.map((item, key) => (
                            <img
                                key={`content_items_img_${key + 1}`}
                                src={
                                    item.item_no === 1
                                        ? imageList['white_box']
                                        : imageList['blue_box']
                                }
                                alt={item.item_no}
                            />
                        ))}
                </div>

                <div className='content_items'>
                    <div>
                        <div className='box_name'>
                            กล่อง{' '}
                            {props.item_get &&
                                props.item_get.map((item, key) => {
                                    return (
                                        <span key={`historydesc_${key + 1}`}>
                                            {' ' + item.item_name}
                                        </span>
                                    );
                                })}
                        </div>
                        <div className='box_send'>
                            ไอเทมจะถูกส่งไปยังกล่องจดหมาย
                        </div>
                        <img
                            onClick={() => props.setValues({ modal_open: '' })}
                            src={imageList['confirm_btn']}
                            alt=''
                        />
                    </div>
                </div>
            </ModalMessageContent>
        </ModalCore>
    );
};

const mapStateToProps = (state) => ({ ...state.Main });
const mapDispatchToProps = { setValues };

export default connect(mapStateToProps, mapDispatchToProps)(CPN);

const ModalMessageContent = styled.div`
    position: relative;
    /* width: calc(100vw / 1920 * 1387); */
    height: calc(100vh / 1080 * 733);
    color: #ffffff;
    box-sizing: border-box;
    flex-direction: column;
    /* word-break: break-word; */
    display: flex;
    /* justify-content: center; */
    align-items: center;
    .content_items {
        width: 100%;
        text-align: center;
        /* display: flex;
        justify-content: center; */
        &:nth-child(1) {
            img {
                width: 20%;
                padding-top: 3%;
            }
        }
        &:nth-child(2) {
            img {
                width: 15%;
                margin: 5% 2% 0;
            }
        }
        &:nth-child(3) {
            > div {
                width: 100%;
                text-align: center;
                padding-top: 5%;
                .box_name {
                    margin-left: 0.5%;
                    font-size: 1em;
                    display: flex;
                    justify-content: center;
                    font-size: 2em;

                    > span {
                        &:not(:last-child):after {
                            content: ' + ';
                        }
                    }
                }
                .box_send {
                    font-size: 1em;
                }
                div {
                    line-height: 1.5;
                    justify-content: center;
                    display: flex;
                    margin-left: 0.5%;

                    > span {
                        margin: 0 0.5%;

                        /* &:not(:last-child):after {
                            content: ' + ';
                        } */
                    }
                }
                img {
                    width: 22%;
                    padding-top: 3%;
                    cursor: pointer;
                }
            }
        }
    }
    /* .congrats_text {
        width: 100%;
        top: 0px;
        position: absolute;
        img {
            position: relative;
            width: 35%;
            left: 50%;
            transform: translate(-50%, 24%);
        }
    } */
`;

const Btn = styled.img`
    display: block;
    margin: 0 auto;
    cursor: pointer;
    max-width: 37%;
    max-height: 20%;
`;

const ItemHistory = styled.div`
    /* display: flex;
    width: 100%;

    .history__content {
        flex: 1;
        position: relative;
        display: flex;
        justify-content: center;

        img {
            width: 20vmax;
            height: auto;
            position: relative;
            padding: 0 10%;
        }
    } */
`;

import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ModalCore from './ModalCore';
import { setValues } from '../../store/redux';
import { imageList } from '../../constants/Import_Images';

const CPN = (props) => {
    return (
        <ModalCore
            modal_name='policy_after_login'
            allowOutsideClicked={true}
            close_btn={true}
        >
            <ModalContent>
                <TitleBox>กติกาการรับไอเทม</TitleBox>
                <div className='content h2p'>
                    <ul>
                        <li>
                            <strong>ระยะเวลากิจกรรม</strong>
                        </li>
                        <br />
                        <li>
                            ตั้งแต่ วันที่ 8 กรกฎาคม 2563 เวลา 00.00 น.
                            ถึงวันที่ 9 ตุลาคม 2563 เวลา 23.00 น.
                        </li>
                        <br />
                        <li>
                            กิจกรรมพิเศษ :
                            ผู้เข้าร่วมกิจกรรมภายในระยะเวลาที่กำหนดนี้จะได้รับไอเทมที่แตกต่างไม่ซ้ำกัน
                            เมื่อเปิดไอเทม "B'lue Legendary Box" ตั้งแต่ วันที่
                            3 กันยายน 2563 เวลา 12.00 น. ถึง วันที่ 9 กันยายน
                            2563 เวลา 23.59 น.เท่านั้น
                        </li>
                        <br />
                        <li>
                            <strong>
                                กติกาการเข้าร่วมกิจกรรม “B’lue x FREE FIRE”
                            </strong>
                        </li>
                        <br />
                        <li>
                            1.
                            ผู้ร่วมรายการสามารถรับรหัสตัวเลขจากการซื้อผลิตภัณฑ์น้ำผสมวิตามิน
                            B’lue รสเครซี่ แคกตัส ที่มีฉลากลาย FREE FIRE มูลค่า
                            25 บาท
                        </li>
                        <br />
                        <li>
                            2. ส่งรหัสตัวเลขหลังฉลากผลิตภัณฑ์ที่ได้รับ (จำนวน 10
                            หลัก) เพื่อรับไอเทม “B’lue Ultimate Box” และ/หรือ
                            “B’lue Legendary Box” โดยมีรายละเอียด ดังนี้
                        </li>
                        <li>
                            2.1 ส่งรหัสตัวเลขจำนวน 1 รหัส เพื่อรับไอเทม “B’lue
                            Ultimate Box” จำนวน 1 ชิ้น
                            โดยผู้ร่วมรายการมีสิทธิ์ได้รับของรางวัลภายในไอเทมดังกล่าวซึ่งประกอบไปด้วย
                        </li>
                        <li className='out_dent'>
                            - ชุด Blue O’ Bell บลูโอเบล มูลค่า 500 บาท
                        </li>
                        <li className='out_dent'>
                            - ตราล่าสังหาร 1 ชิ้น มูลค่า 6 บาท
                        </li>
                        <li className='out_dent'>
                            - แผนที่เสบียง 1 ชิ้น มูลค่า 6 บาท
                        </li>
                        <li className='out_dent'>
                            - เรียกแอร์ดรอป 1 ชิ้น มูลค่า 5 บาท
                        </li>
                        <li className='out_dent'>
                            - โกลด์ 300 หน่วย มูลค่า 2 บาท
                        </li>
                        <li>
                            2.2 เมื่อส่งรหัสตัวเลขครบทุกๆ จำนวน 5 รหัส
                            ผู้ร่วมรายการจะได้รับไอเทม “B’lue Legendary Box”
                            จำนวน 1 ชิ้น
                            ผู้ร่วมรายการมีสิทธิ์ได้รับของรางวัลภายในไอเทมดังกล่าวซึ่งประกอบไปด้วย
                        </li>
                        <li className='out_dent'>
                            - หน้ากาก Blue O’ Bell บลูโอเบล มูลค่า 125 บาท
                        </li>
                        <li className='out_dent'>
                            - เสื้อ Blue O’ Bell บลูโอเบล มูลค่า 125 บาท
                        </li>
                        <li className='out_dent'>
                            - กางเกง Blue O’ Bell บลูโอเบล มูลค่า 125 บาท
                        </li>
                        <li className='out_dent'>
                            - รองเท้า Blue O’ Bell บลูโอเบล มูลค่า 125 บาท
                        </li>
                        <br />
                        <li>
                            3. บริษัทฯ
                            ขอสงวนสิทธิ์ให้ของรางวัลแก่ผู้ร่วมรายการที่เข้าสู่ระบบด้วยบัญชีของ
                            Free Fire ที่ผูกไว้กับบัญชี Facebook, Google, หรือ
                            VK เท่านั้น
                        </li>
                        <li>
                            4. ผู้ร่วมรายการจะได้รับไอเทม “B’lue Ultimate Box”
                            และ/หรือ “B’lue Legendary Box” เข้ากล่องจดหมายในเกม
                            FREE FIRE ภายหลังจากส่งรหัสหลังฉลากเรียบร้อยแล้ว
                        </li>
                        <li>
                            5.
                            ผู้ร่วมรายการสามารถส่งรหัสได้ไม่จำกัดจำนวนครั้งต่อวัน
                        </li>
                        <li>
                            6. ในกรณีที่ผู้ร่วมรายการส่งรหัสซ้ำ
                            หรือกรอกรายละเอียดไม่ถูกต้องเกินจำนวนครั้งที่กำหนด
                            การีนาขอสงวนสิทธิ์ในการไม่อนุญาตให้ผู้ร่วมรายการส่งรหัสได้
                        </li>
                        <li>
                            7. มูลค่าของรางวัลเป็นมูลค่า ณ วันที่ 8 กรกฎาคม พ.ศ.
                            2563
                        </li>
                        <br />
                        <li>
                            <strong>ข้อกำหนดและเงื่อนไข</strong>
                        </li>
                        <br />
                        <li>
                            1.
                            ผู้ร่วมกิจกรรมทำตามเงื่อนไขและกติกาครบถ้วนและถูกต้องเท่านั้น
                            ถึงจะมีสิทธิ์รับของรางวัล หากตรวจพบว่า
                            มีการทุจริตทุกประการเกิดขึ้น ไม่ว่าในกรณีใดกรณีหนึ่ง
                            ทางบริษัทฯ
                            ขออนุญาตทำการสละสิทธิ์การเข้าร่วมกิจกรรมและการรับของรางวัลโดยไม่ต้องแจ้งให้ทราบล่วงหน้า
                            ทั้งนี้ทั้งนั้นผู้เข้าร่วมกิจกรรมนี้
                            ได้รับทราบและยอมรับกติกาและเงื่อนไขต่างๆ ของบริษัทฯ
                            เป็นอย่างดี ก่อนเข้าร่วมกิจกรรมครั้งนี้
                        </li>
                        <li>
                            2.
                            ของรางวัลที่ได้รับไม่สามารถจำหน่ายหรือแลกเปลี่ยนเป็นเงินสดหรือของรางวัลอื่นได้
                            และไม่สามารถโอนสิทธิ์ในการได้รับของรางวัลให้กับบุคคลอื่นได้ไม่ว่ากรณีใด
                        </li>
                        <li>
                            3.
                            การีนาขอสงวนสิทธิ์ไม่รับผิดชอบต่อความผิดพลาดอันเกิดขึ้นจากระบบอินเทอร์เน็ตที่ล่าช้า
                            ไม่สมบูรณ์
                            หรือเกิดจากการฉ้อโกงหรือการส่งข้อมูลที่ไม่ถูกต้อง
                            ที่เกิดจากผู้เข้าร่วมกิจกรรม
                            ไม่ว่าจะเป็นเหตุที่เกิดจากความผิดพลาด การเพิกเฉย
                            การแก้ไขปรับเปลี่ยน การให้สินบน การลบทิ้ง ขโมย
                            การทำลายข้อมูลโดยไม่ได้รับอนุญาตหรือการลักลอบใช้ข้อมูล
                            ความเสียหายของข้อมูล เครือข่ายล้มเหลว
                            หรือความผิดพลาดของซอฟต์แวร์หรือฮาร์ดแวร์
                            หรือไม่ว่าด้วยเหตุใดก็ตาม
                        </li>
                        <li>
                            4.
                            การีนาขอสงวนสิทธิ์ในการเปลี่ยนแปลงรายละเอียดของกิจกรรมโดยไม่ต้องแจ้งให้ทราบล่วงหน้า
                        </li>
                        <li>
                            5. การีนาขอสงวนสิทธิ์ในการใช้ภาพ เสียง วิดีโอ
                            และ/หรือข้อมูลอื่นใดของผู้ร่วมกิจกรรมได้
                            ไม่จำกัดรูปแบบสื่อ ระยะเวลา
                            โดยไม่ต้องเสียค่าตอบแทนใดๆ
                            ซึ่งถือเป็นสิทธิ์ขาดของการีนาที่จะสามารถนำไปเผยแพร่
                            และประชาสัมพันธ์ได้
                            โดยไม่ต้องขออนุญาตและ/หรือแจ้งให้ทราบล่วงหน้า
                        </li>
                        <li>6. คำตัดสินของการีนาถือเป็นที่สิ้นสุด</li>
                    </ul>
                </div>
            </ModalContent>
        </ModalCore>
    );
};

const mapStateToProps = (state) => ({ ...state.Main });
const mapDispatchToProps = { setValues };

export default connect(mapStateToProps, mapDispatchToProps)(CPN);

const ModalContent = styled.div`
    position: relative;
    width: calc(100vw / 1920 * 1382);
    height: calc(100vh / 1080 * 733);
    background: top center no-repeat url(${imageList['bg_modal']});
    background-size: 100% 100%;
    color: #ffffff;
    word-break: break-word;
    padding: 4%;
    box-sizing: border-box;
    margin: 0 auto;
    font-size: 0.6em;
    .text {
        &--green {
            color: #1b361e;
        }
        &--blue {
            color: #0b2e47;
        }
        &--purple {
            color: #460b3c;
        }
        &--gold {
            color: #bc4c00;
        }
    }
    .inner_title {
        width: 100%;
        height: 20%;
        > img {
            display: block;
            margin: 0 auto;
        }
    }
    .content {
        width: 100%;
        height: 60%;
        padding: 1% 0;
        margin: 3% auto 0;
        text-align: center;
        color: #000000;
        &.h2p {
            text-align: left;
            color: #ffffff;
            @media (max-width: 1024px) {
                font-size: 2em;
            }
            h4 {
                margin: 0 0 1em 0;
                text-align: center;
            }
            ul {
                width: 96%;
                height: 100%;
                overflow: auto;
                margin: 0;
                padding-left: 4%;
                border-radius: 5px;
                padding-right: 3%;
                font-size: 2em;
                list-style-type: none;

                @media (max-width: 1024px) {
                    font-size: 0.7em;
                }
                div {
                    text-align: left;
                    font-size: 1em;
                    font-weight: bold;
                    margin: 1% auto;
                }
                &::-webkit-scrollbar {
                    width: 15px;
                    background: #2c5a84;
                    @media (max-width: 1024px) {
                        width: 10px;
                    }
                }
                &::-webkit-scrollbar-thumb {
                    background: #fff;
                }
                > li {
                    width: 100%;
                    text-align: left;
                    line-height: 1.5;
                    &.out_dent {
                        padding-left: 1em;
                    }
                }
            }
        }
    }
`;
const TitleBox = styled.div`
    display: block;
    margin: 0 auto;
    font-size: 4.5em;
    color: #ffffff;
    text-align: center;
    height: 15%;
    line-height: 1;
    > img {
        display: block;
        margin: 0 auto;
    }

    @media (max-width: 1024px) {
        font-size: 3em;
    }
`;
const Btns = styled.img`
    display: block;
    margin: 2% auto 0;
    max-width: 40%;
    cursor: pointer;
`;

import React, { useState } from 'react';
import { animated, useSpring, config } from 'react-spring';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { setValues } from '../../store/redux';

const Main = (props) => {
    return (
        <ProgressWrapper>
            {props.blueBox ? (
                <span>จะได้โบนัสใน:{props.item_count}/5</span>
            ) : (
                <span>ค้นพบแล้ว</span>
            )}

            {props.blueBox ? (
                <ProgressBar>
                    <ProgressAchieved
                        // style={{ width: `${props.milestone}%` }}
                        milestone={props.milestone}
                    />
                </ProgressBar>
            ) : (
                <ProgressBar>
                    <ProgressAchieved style={{ width: '100%' }} />
                    {props.blueBox ? (
                        ''
                    ) : (
                        <div className='ItemBox'>{props.all_redeem} กล่อง</div>
                    )}
                </ProgressBar>
            )}
        </ProgressWrapper>
    );
};

const mapStateToProps = (state) => ({ ...state.Main });

const mapDispatchToProps = { setValues };

export default connect(mapStateToProps, mapDispatchToProps)(Main);

const ProgressWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 88%;
    height: 27%;
    text-align: center;
    line-height: 1;

    span {
        font-size: 1em;
    }
`;

const ProgressBar = styled.div`
    display: flex;
    height: 40px;
    border: 3px solid #fff;
    border-radius: 20px;
    padding: 2px;

    @media (max-width: 767px) {
        height: 20px;
        border: 2px solid #fff;
    }

    .ItemBox {
        display: flex;
        justify-content: center;
        width: 100%;
        position: absolute;
        align-self: center;
        font-size: 1em;
        color: #000;
    }
`;

const ProgressAchieved = styled.div`
    display: flex;
    background: #fff;
    border-radius: 20px;
    width: ${(props) =>
        props.milestone === 100 ? '0%' : props.milestone + '%'};
`;

const ProgressToGo = styled.div`
    display: flex;
`;

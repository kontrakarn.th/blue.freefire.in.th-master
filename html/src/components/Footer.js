import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { setValues } from '../store/redux';
import { imageList } from './../constants/Import_Images';

const Main = (props) => {
    return (
        <Footer>
            <img className='left' src={imageList['footer_left']} alt='' />
            {props.play_animation && (
                <img
                    className='center'
                    src={imageList['skip_animation_text']}
                    alt='skip_animation_text'
                />
            )}
            <img className='right' src={imageList['footer_right']} alt='' />
        </Footer>
    );
};

const mapStateToProps = (state) => ({ ...state.Main });

const mapDispatchToProps = { setValues };

export default connect(mapStateToProps, mapDispatchToProps)(Main);

const Footer = styled.div`
    position: absolute;
    bottom: 1.5%;
    left: 0;
    right: 0;
    width: 97%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 0 auto;
    .left {
        max-width: 18%;
        z-index: 10;
    }
    .center {
        width: 12%;
        position: absolute;
        left: 50%;
        transform: translateX(-50%);
        z-index: 10;
    }
    .right {
        max-width: 23%;
        z-index: 10;
    }
`;

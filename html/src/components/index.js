import DecorContainerLeft from './DecorContainerLeft';
import DecorContainerRight from './DecorContainerRight';
import Footer from './Footer';
import ContentCodeContainer from './ContentCodeContainer';

export {
    DecorContainerLeft,
    DecorContainerRight,
    Footer,
    ContentCodeContainer,
};

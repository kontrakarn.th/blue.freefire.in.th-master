import React, { useState, useEffect } from 'react';
import styled, { keyframes } from 'styled-components';

import { imageList } from './../constants/Import_Images';

export default (props) => {
    return (
        <DecorContainerRight>
            <Decor src={imageList['submarine']} alt='' />
            <img src={imageList['submarine_circle']} alt='status_collection' />
        </DecorContainerRight>
    );
};

const Rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const DecorContainerRight = styled.div`
    width: 27%;
    right: 2.5%;
    top: 19%;
    max-width: 100%;
    position: absolute;
    z-index: 2;

    img {
        &:nth-child(1) {
            left: 5.4%;
            position: relative;
            top: 3px;
            z-index: 1;
        }
        &:nth-child(2) {
            animation: ${Rotate} 8s infinite linear;
            left: 24%;
            position: absolute;
            top: 2%;
            width: 61%;
        }
    }
`;

const Decor = styled.img`
    position: initial;
    pointer-events: none;
    &.left {
    }
    &.right {
        right: 3%;
        top: 23%;
        max-width: 25%;
    }
`;

import React, { useState, useEffect } from 'react';
import styled, { keyframes } from 'styled-components';

import { imageList } from './../constants/Import_Images';

export default (props) => {
    return (
        <DecorContainerLeft>
            <Decor
                className='left'
                src={imageList['status_collection']}
                alt=''
            />
            <img src={imageList['circle_main']} alt='status_collection' />

            <img src={imageList['circle']} alt='status_collection' />
            <img src={imageList['circle']} alt='status_collection' />
            <img src={imageList['circle']} alt='status_collection' />
            <img src={imageList['circle']} alt='status_collection' />
        </DecorContainerLeft>
    );
};

const Rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const DecorContainerLeft = styled.div`
    width: 27%;
    left: 1.5%;
    top: 19%;
    max-width: 100%;
    position: absolute;
    z-index: 2;

    img {
        &:nth-child(1) {
            left: 5.4%;
            position: relative;
            top: 3px;
            z-index: 1;
        }
        &:nth-child(2) {
            animation: ${Rotate} 8s infinite linear;
            position: absolute;
            width: 57%;
            left: 16%;
            top: 18%;
        }
        &:nth-child(3) {
            animation: ${Rotate} 8s infinite linear;
            width: 24%;
            left: 3.5%;
            position: absolute;
        }
        &:nth-child(4) {
            animation: ${Rotate} 8s infinite linear;
            width: 24%;
            top: 8%;
            right: 2%;
            position: absolute;
        }
        &:nth-child(5) {
            animation: ${Rotate} 8s infinite linear;
            width: 25%;
            top: 65%;
            left: 0%;
            position: absolute;
        }
        &:nth-child(6) {
            animation: ${Rotate} 8s infinite linear;
            width: 25%;
            bottom: 3.5%;
            right: 10%;
            position: absolute;
        }
    }
`;

const Decor = styled.img`
    position: initial;
    pointer-events: none;
    &.left {
    }
    &.right {
        right: 3%;
        top: 23%;
        max-width: 25%;
    }
`;

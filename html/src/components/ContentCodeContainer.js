import React, { useState, useEffect } from 'react';
import styled, { keyframes } from 'styled-components';
import { connect } from 'react-redux';

import { setValues, goRedeem, goHistory } from '../store/redux';
import { imageList } from './../constants/Import_Images';
import CountDown from '../features/Countdown';
import Progress from '../features/Progress';

const Main = (props) => {
    let timer;
    const redeemCode = () => {
        if (props.ban) {
            props.setValues({
                modal_open: 'message',
                modal_message: `คุณถูกแบน จะกรอกโค้ดได้อีกครั้ง ${props.ban_end}`,
            });
            return;
        }

        if (props.play_animation === false) {
            if (props.redeem_code === '' || props.redeem_code.length === 0) {
                props.setValues({
                    modal_open: 'message',
                    modal_message: 'โปรดกรอกรหัส',
                });
            } else {
                props.goRedeem({
                    jwtToken: props.jwtToken,
                    code: props.redeem_code,
                });
            }
        }
    };

    const openHistory = () => {
        if (props.history && props.history.length === 0) {
            props.goHistory(props.jwtToken);
        } else {
            props.setValues({ modal_open: 'history' });
        }
    };

    const handleCancelAnimation = () => {
        if (props.play_animation) {
            clearTimeout(timer);
            props.setValues({
                play_animation: false,
                modal_open: 'reward',
            });
        }
    };

    useEffect(() => {
        if (props.play_animation) {
            timer = setTimeout(() => {
                props.setValues({
                    play_animation: false,
                    modal_open: 'reward',
                    redeem_code: '',
                });
            }, 14000);
        }
    });

    return (
        <ContentCodeContainer
            onAnimate={props.play_animation}
            onClick={() => handleCancelAnimation()}
        >
            <ContentCodeHeader>
                <SubmarineTop src={imageList['submarine_top']} alt='' />
                <ItemTopContainer>
                    <div className='box'>
                        <div className='box__uid'>
                            <div className='box__uid--image'>
                                <img src={imageList['uid_cover']} alt='' />
                            </div>
                            <div className='box__uid--textuid'>
                                <span>UID : </span>
                                {props.account_id}
                            </div>
                        </div>
                        <div className='box_welcome--text'>
                            <span>WELCOME : </span> {props.account_name}
                        </div>
                    </div>
                    <div className='box countdown_bar'>
                        <img
                            src={imageList['countdown_bar']}
                            alt='countdown_bar'
                        />
                        <CountDown deadline={props.end_at} />
                    </div>
                    <div className='box action__btn'>
                        <div onClick={() => openHistory()}>
                            <img src={imageList['history_btn']} alt='' />
                        </div>
                        <div
                            onClick={() =>
                                props.setValues({
                                    modal_open: 'policy_after_login',
                                })
                            }
                        >
                            <img src={imageList['how_to_btn']} alt='' />
                        </div>
                        <div
                            onClick={() =>
                                props.setValues({ authState: 'LOGGED_OUT' })
                            }
                        >
                            <img src={imageList['log_out_btn']} alt='' />
                        </div>
                    </div>
                </ItemTopContainer>
            </ContentCodeHeader>
            <ContentCodeContent
                className={props.play_animation ? 'hologram_fade_out' : ''}
            >
                <div className='hologram'>
                    <img src={imageList['hologram']} alt='hologram' />
                    <img
                        src={imageList['hologram_item1']}
                        alt='hologram_item1'
                    />
                    <img
                        src={imageList['hologram_item2']}
                        alt='hologram_item2'
                    />
                    <img
                        src={imageList['hologram_item3']}
                        alt='hologram_item3'
                    />
                </div>
                <div className='inputcode'>
                    <div className='inputcode__text'>กรอกโค้ด</div>
                    <div className='inputcode__form_input'>
                        <input
                            autoComplete='off'
                            type='text'
                            maxLength='10'
                            placeholder='กรุณากรอกรหัส 10 หลัก'
                            value={props.redeem_code}
                            onChange={(e) =>
                                props.setValues({ redeem_code: e.target.value })
                            }
                        />
                        {/* <input {...code} name='code' autocomplete='off' /> */}
                    </div>
                </div>
            </ContentCodeContent>
            <ContentCodeFooter>
                {/* <Progress></Progress> */}
                <div className='blue_box'>
                    <div>
                        <img src={imageList['blue_box']} alt='' />
                    </div>
                    <div>
                        <Progress blueBox />
                    </div>
                </div>
                <div
                    onClick={() => redeemCode()}
                    className={
                        props.play_animation
                            ? 'code__btn hologram_fade_out'
                            : 'code__btn'
                    }
                >
                    <img src={imageList['confirm_btn']} alt='confirm_btn' />
                </div>
                <div className='white_box'>
                    <div>
                        <Progress />
                    </div>
                    <div>
                        <img width='70%' src={imageList['white_box']} alt='' />
                    </div>
                </div>
            </ContentCodeFooter>
            <ArmLeft className={props.play_animation ? 'arm_left_in' : ''}>
                <img
                    className='arm_left'
                    src={imageList['arm_left']}
                    alt='arm_left'
                />
            </ArmLeft>
            <ArmRight className={props.play_animation ? 'arm_right_in' : ''}>
                <img
                    className='arm_right'
                    src={imageList['arm_right']}
                    alt='arm_right'
                />
            </ArmRight>
            <ScreenContainer
                active={props.play_animation}
                src={imageList['screen']}
            ></ScreenContainer>
            <UnderWater clasName='underwater' active={props.play_animation}>
                <img
                    className='underwater__light'
                    src={imageList.watersurface}
                    alt='bg_under_water'
                />
                <img
                    className='underwater__floor'
                    src={imageList.bg_under_water}
                    alt='bg_under_water'
                />

                <div className='underwater__reward'>
                    <img
                        className={
                            props.play_animation
                                ? 'b_arm_left b_arm_left_in'
                                : 'b_arm_left'
                        }
                        src={imageList['b_arm_left']}
                        alt='b_arm_left'
                    />
                    <img
                        className='reward_box_img'
                        src={
                            props.item_get && props.item_get.length === 1
                                ? imageList.white_box_underwater
                                : imageList.blue_box_underwater
                        }
                        alt='underwater_box'
                    />
                    <img
                        className={
                            props.play_animation
                                ? 'b_arm_right b_arm_right_in'
                                : 'b_arm_right'
                        }
                        src={imageList['b_arm_right']}
                        alt='b_arm_right'
                    />
                </div>
            </UnderWater>
            <img
                style={{ pointerEvents: 'none' }}
                className='black_box_left'
                src={imageList['black_box_left']}
                alt='black_box_left'
            />
            <img
                style={{ pointerEvents: 'none' }}
                className='black_box_right'
                src={imageList['black_box_right']}
                alt='black_box_right'
            />
        </ContentCodeContainer>
    );
};

const mapStateToProps = (state) => ({ ...state.Main });

const mapDispatchToProps = { setValues, goRedeem, goHistory };

export default connect(mapStateToProps, mapDispatchToProps)(Main);

const ItemTopContainer = styled.div`
    width: 100%;
    z-index: 3;
    display: flex;
    justify-content: space-around;
    align-items: center;
    height: 60%;
    text-align: center;
    font-size: 0.8em;

    .box {
        justify-content: center;

        .box__uid {
            position: relative;
            z-index: 1;
            margin-right: 5%;
            color: #00ffff;

            @media (max-width: 1024px) {
                width: 50%;
            }

            .box__uid--image {
                z-index: 2;
                display: contents;
                img {
                    transform: translateY(-15%);
                    width: 100%;
                }
            }

            .box__uid--textuid {
                z-index: 3;
                position: absolute;
                top: 0px;
                display: flex;
                width: 100%;
                justify-content: center;

                @media (max-width: 1024px) {
                    font-size: 0.8em;
                }

                @media (min-width: 1025px) and (max-width: 1120px) {
                    font-size: 0.7em;
                }

                @media (max-width: 1024px) {
                    font-size: 0.8em;
                }

                > span {
                    margin-right: 3%;
                    color: #fff;
                }
            }
        }

        .box_welcome--text {
            @media (min-width: 1025px) and (max-width: 1120px) {
                font-size: 0.7em;
            }

            @media (max-width: 1024px) {
                font-size: 0.8em;
            }
            > span {
                color: #00ffff;
            }
        }

        &:nth-child(1) {
            flex: 1 1 0;
            display: flex;
            flex-direction: row;
            height: 29%;
        }
        &:nth-child(2) {
            flex: 1 1 0;
        }
        &:nth-child(3) {
            flex: 1 1 0;
            display: flex;

            div {
                cursor: pointer;
                margin: 0 10px;
            }
        }
    }

    .countdown_bar {
        position: relative;
        width: 34%;
        height: 51%;
        display: flex;
        justify-content: space-around;
        align-items: center;

        img {
            position: absolute;
        }

        .timer {
            position: absolute;
            display: flex;
            width: 100%;
            justify-content: center;

            .timer__unit--container {
                display: flex;
                flex-direction: row;
                width: 70%;
                justify-content: space-evenly;
                font-size: 1em;
                pointer-events: none;

                .timer__unit--color {
                    color: #00ffff;
                }

                div {
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                    line-height: 0.7;
                    align-self: center;

                    span {
                        text-transform: uppercase;
                        font-size: 0.4em;
                        color: #fff;
                    }
                }
            }
        }
    }
`;

const Flicker = keyframes`
    0% { opacity: 0.5 }
		50% { opacity: 1 }
		100% { opacity: 0.5 }
`;

const Scaling = keyframes`
  0% {
      transform: scale(1);
  }
  25% {
    transform: scale(1);
  }
  50% {
    transform: scale(.5);
  }
  75% {
      transform: scale(.5);
  }
  100% {
      transform: scale(1);
  }
`;

const ScalingReverse = keyframes`
  0% {
      transform: scale(.5);
  }
  25% {
    transform: scale(.5);
  }
  50% {
    transform: scale(1);
  }
  75% {
      transform: scale(1);
  }
  100% {
      transform: scale(.5);
  }
`;

const WaterSurfaceUp = keyframes`
  from {
      top: 9%;
  }
  to {
      top: -42%;
  }
`;

const HologramFadeOut = keyframes`
  from {
      opacity: 1;
  }
  to {
      opacity: 0;
  }
`;

const ScreenUp = keyframes`
  from {
      transform: translate(-50%, 100%);
  }
  to {
      transform: translate(-50%, 0%);
  }
`;

const RewardBoxUp = keyframes`
  from {
      transform: translate(-50%, 800%);
  }
  to {
      transform: translate(-50%, -50%);
  }
`;

const ArmLeftIn = keyframes`
  0% {
      transform: translateX(-100%);
  }
  80% {
      transform: translateX(-100%);
  }
  100% {
      transform: translateX(0%);
  }
`;

const ArmRightIn = keyframes`
  0% {
      transform: translateX(100%);
  }
  80% {
      transform: translateX(100%);
  }
  100% {
      transform: translateX(0%);
  }
`;

const BArmLeftIn = keyframes`
  0% {
      transform: translateX(-500%);
  }
  80% {
      transform: translateX(-500%) translateY(-51%);
  }
  100% {
      transform: translateX(-51%) translateY(-51%);
  }
`;

const BArmRightIn = keyframes`
  0% {
      transform: translateX(500%);
  }
  80% {
      transform: translateX(500%) translateY(-59%);
  }
  100% {
      transform: translateX(50%) translateY(-59%);
  }
`;

const ContentCodeContainer = styled.div`
    margin: 0px auto;
    position: absolute;
    top: 0;
    display: flex;
    width: 100%;
    height: 100%;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    cursor: ${(props) => (props.onAnimate ? 'pointer' : '')};

    .watersurface_up {
        animation: ${WaterSurfaceUp} 7s ease-in;
        top: -42%;
    }

    .hologram_fade_out {
        animation: ${HologramFadeOut} 2s ease-in;
        opacity: 0;
    }
    .screen_up {
        animation: ${ScreenUp} 10s ease-in;
        transform: translateY(0%);
    }
    .reward_box_up {
        /* animation: ${RewardBoxUp} 10s ease-in; */
        transform: translate(-50%, -50%);
    }
    .arm_left_in {
        animation: ${ArmLeftIn} 12s ease-in;
        transform: translateX(0%);
    }
    .arm_right_in {
        animation: ${ArmRightIn} 12s ease-in;
        transform: translateX(0%);
    }
    .b_arm_left_in {
        animation: ${BArmLeftIn} 12s ease-in;
        transform: translateX(-51%) translateY(-51%);
    }
    .b_arm_right_in {
        animation: ${BArmRightIn} 12s ease-in;
        transform: translateX(50%) translateY(-59%);
    }
    .black_box_left {
        position: absolute;
        bottom: 0px;
        left: 0px;
        width: 14.5%;
        z-index: 5;
    }
    .black_box_right {
        position: absolute;
        bottom: 0px;
        right: 0px;
        width: 14.5%;
        z-index: 5;
    }
`;

const ContentCodeHeader = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    height: 24%;
`;

const ContentCodeContent = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    height: 53%;
    position: relative;

    .hologram {
        position: absolute;
        margin: 0 auto;
        width: 38%;
        z-index: 2;
        pointer-events: none;
        img {
            &:nth-child(1) {
                margin: 0 auto;
                width: 100%;
                animation: ${Flicker} 10s infinite;
            }
            &:nth-child(2) {
                position: absolute;
                left: 6%;
                top: 38%;
                width: 25%;
                animation: ${ScalingReverse} 10s infinite;
                transform-origin: bottom right;
            }
            &:nth-child(3) {
                position: absolute;
                width: 25%;
                left: 20%;
                top: 5%;
                animation: ${Scaling} 10s infinite;
                transform-origin: bottom right;
            }
            &:nth-child(4) {
                position: absolute;
                width: 40%;
                right: -8%;
                top: 10%;
                animation: ${Scaling} 10s infinite;
                transform-origin: bottom left;
            }
        }
    }

    .inputcode {
        position: absolute;
        bottom: 0;
        width: 42%;
        z-index: 1;

        .inputcode__text {
            text-align: center;
            font-size: 1em;
            position: relative;
            line-height: 0.8;
        }
        .inputcode__form_input {
            font-size: 1em;
            color: #000;
            position: relative;
            width: 100%;

            input {
                text-align: center;
                width: 100%;
            }
        }
    }
`;

const ContentCodeFooter = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;
    height: 23%;
    z-index: 6;
    /* pointer-events: none; */

    .blue_box {
        width: 33.33333%;
        display: flex;
        align-content: center;
        align-items: flex-start;
        align-self: baseline;
        > div {
            &:nth-child(1) {
                margin-right: 3%;
                img {
                    width: 10vmax;
                    padding-left: 20px;
                }
            }
            &:nth-child(2) {
                width: 50%;
            }
        }
    }

    .white_box {
        width: 33.33333%;
        display: flex;
        align-content: center;
        align-items: flex-start;
        align-self: flex-start;
        justify-content: flex-end;
        > div {
            &:nth-child(1) {
                width: 50%;
                justify-content: flex-end;
                display: flex;
                position: relative;
                > div {
                    width: 100%;
                }
            }
            &:nth-child(2) {
                margin-left: 3%;
                img {
                    width: 10vmax;
                    padding-right: 20px;
                }
            }
        }
    }

    .code__btn {
        width: 22%;
        margin-top: 2%;

        img {
            cursor: pointer;
            width: 100%;
        }
    }
`;

const SubmarineTop = styled.img`
    position: absolute;
    z-index: 2;
    pointer-events: none;
`;

const WaterSurface = keyframes`
  0% {
      opacity: 1;
  }
  50% {
    opacity: .7;
  }
  100% {
      opacity: 1;
  }
`;

const ArmLeft = styled.div`
    position: absolute;
    left: 0;
    z-index: 4;
    width: 45%;
    bottom: 9%;
    transform: translateX(-100%);
`;

const ArmRight = styled.div`
    position: absolute;
    right: 0;
    z-index: 3;
    width: 45%;
    bottom: 6%;
    transform: translateX(100%);
`;

const ScreenContainer = styled.img`
    /* justify-content: center; */
    position: absolute;
    display: block;
    width: calc(100% / 1920 * 1120);
    left: 50%;
    bottom: 0;
    z-index: 2;
    transform: translate(-50%, 100%);

    animation: ${(props) => (props.active ? ScreenUp : 'none')} 2s ease-out
        alternate;
    animation-delay: 2s;
    animation-fill-mode: forwards;
    /* img {
        position: relative;
        width: 65%;
    } */
`;

const SlideUp = keyframes`
  from {
      transform: translateY(0%);
  }
  to {
      transform: translateY(-50%);
  }
`;

const UnderWater = styled.div`
    position: absolute;
    display: block;
    width: 100%;
    animation: ${(props) =>
        props.active ? SlideUp : 'none'} 6s linear alternate;
    animation-delay: 4s; 
    animation-fill-mode: forwards;
    /* hologram 2s, mornitor 2s */

    top: 0;
    left: 0;
    padding-top: calc(200% / 16 * 9);
    /* transform: translateY(100%); */
    /* screen_up */

    /* animation: ${SlideUp} 10s linear; */

    .underwater {
        &__light {
            position: absolute;
            left: 0;
            top: 0;
            display: block;
            width: 100%;
            animation: ${WaterSurface} 6s infinite;
            pointer-events: none;
        }
        &__floor {
            position: absolute;
            left: 0;
            bottom: 0;
            display: block;
            width: 100%;
        }
        &__reward {
            position: absolute;
            left: 0;
            bottom: 0;
            display: flex;
            width: 100%;
            height: 50%;
            justify-content: center;
            align-items: center;

            .reward_box_img {
                position: absolute;
                z-index: 2;
                width: 23%;
            }

            .b_arm_right {
                width: 10%;
                z-index: 1;
            }

            .b_arm_left {
                width: 10%;
                z-index: 1;
            }
        }
    }
`;

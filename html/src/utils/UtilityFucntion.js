export const numberWithCommas=(x)=> {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export const scrollToElement=(id)=>{
	window.scrollTo({
		top: document.getElementById(id).offsetTop,
		left: 0,
		behavior: 'smooth'
	});
}
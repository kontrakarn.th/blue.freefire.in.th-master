export const isGarenaMobile = () => {
	return /GarenaGas|Garena|Gas/i.test(navigator.userAgent);
}

export const isMobile = () => {
	return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}

export const detectIOS = () => {
    let match = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/), version;

    if (match !== undefined && match !== null) {
        version = [
            parseInt(match[1], 10),
            parseInt(match[2], 10),
            parseInt(match[3] || 0, 10)
        ];
        return parseFloat(version.join('.'));
    }
    return 0;
}

export const isAndroid = () => {
	return /Android/i.test(navigator.userAgent);
}

export const isIphone = () => {
	return /iP(hone|od|od touch|ad)/i.test(navigator.userAgent) && !window.MSStream;
}

export const isFlashEnabled = () => {

  // @see http://stackoverflow.com/questions/159261/cross-browser-flash-detection-in-javascript
  // try to use swfobject first, or use original way
  if(window['swfobject']) {
    if(window['swfobject'].hasFlashPlayerVersion("1")){
      return true;
    } else {
      return false;
    }
  } else if (window.ActiveXObject) {
    var hasFlash = false;
    try {
        var fo = new window.ActiveXObject('ShockwaveFlash.ShockwaveFlash');
        if (fo) hasFlash = true;
    } catch(e) {
        if (navigator.mimeTypes["application/x-shockwave-flash"] !== undefined) hasFlash = true;
    }
    return hasFlash;
  }
}

// @see https://codepen.io/gapcode/pen/vEJNZN
export const detectIE = () => {
	let ua = window.navigator.userAgent;
	let msie = ua.indexOf('MSIE ');
	if (msie > 0) {
		// IE 10 or older => return version number
		return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
	}

	let trident = ua.indexOf('Trident/');
	if (trident > 0) {
	// IE 11 => return version number
		let rv = ua.indexOf('rv:');
		return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
	}

	// other browser
	return false;
}

export const isEdge = () => {
	return /Edge/i.test(navigator.userAgent);
}

export const isSafari = () => {
	return /Safari/i.test(navigator.userAgent) && !/Chrome/i.test(navigator.userAgent);
}

export const isChrome = () => {
	return /Chrome/i.test(navigator.userAgent) && /Google Inc/i.test(navigator.vendor);
}

export const isCanvasSupported = () => {
	let elem = document.createElement('canvas');
	return elem.getContext && elem.getContext('2d');
}
